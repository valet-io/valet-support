<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<style>
	#wrap ul {
		list-style-type: disc;
		list-style-position: inside;
	}
	#wrap ol {
		list-style-type: decimal;
		list-style-position: inside;
	}
	#wrap ul ul,
	#wrap ol ul {
		list-style-type: circle;
		list-style-position: inside;
		margin-left: 15px;
	}
	#wrap ol ol,
	#wrap ul ol {
		list-style-type: lower-latin;
		list-style-position: inside;
		margin-left: 15px;
	}

	.valet-support.connect.success {
		margin-bottom: 10px;
		color: green;
		font-weight: bold;
	}
</style>

<div id="wrap">
	<h1><?php _e( 'Valet Support', 'valet-support' );?></h1>
	<ul>
		<li><a href="<?php echo esc_url( add_query_arg( 'page', 'valet-diagnostics', is_multisite() ? network_admin_url( 'admin.php' ) : admin_url( 'admin.php' ) ) ); ?>">Diagnostics</a></li>
	</ul>

	<h2><?php _e( 'Connect to Central Site', 'valet-support' ); ?></h2>
	<?php
	$is_connected = Valet_Support::get_instance()->is_connected();
	if ( $is_connected ) {
		?>
		<div class="valet-support connect success"><?php _e( 'You are connected to the Central site.', 'valet-central' );?></div>
		<?php
	}
	?>
	<a class="button <?php echo $is_connected ? '' : 'button-primary'; ?>" href="<?php echo esc_url( Valet_Support::get_instance()->get_central_authorization_url() );?>"><?php echo $is_connected ? __( 'Reconnect to the Maintenance Site' ) : __( 'Connect to the Maintenance Site' ); ?></a>
	<?php
    if ( $is_connected ) {
    ?>
        &nbsp;&nbsp;
		<a class="button is-destructive" href="#" onclick="valet_support_disconnect()" style="color:red; border-color:red;"><?php echo __( 'Disconnect from the Maintenance Site', 'valet-central' ); ?></a>
    <?php
    }
    ?>
	<h2><?php _e( 'Useful Links', 'valet-support' ); ?></h2>
	<?php $outside_links = Valet_Support_Requirements::get_instance()->get_required_outside_links(); ?>

	<?php if ( is_array( $outside_links ) ) : ?>
		<ul>
			<?php foreach ( $outside_links as $outside_link ) : ?>
				<li>
					<a href="<?php echo esc_url( $outside_link['url'] ); ?>" target="_blank"><?php echo esc_html( $outside_link['name'] ); ?></a>
				</li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>

	<br />
	<h2><?php _e( 'Dynamic Links', 'valet-support' ); ?></h2>
	<?php
	$is_connected = Valet_Support::get_instance()->is_connected();
	if ( $is_connected ) {
		?>
		<div id="valet-support-links-wrapper"></div>
		<?php
	} else {
		?>
		<div class="valet-support disconnected error">
			<?php
			echo VALET_SUPPORT_CONNECT_AVAILABILITY_ERROR;
			?>
		</div>	
		<?php
		}
	?>
	
</div>
