<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$is_connected = Valet_Support::get_instance()->is_connected();
if ( $is_connected ) {
	?>
	<a href="" id="valet-report-download" target="_blank" style="display: none;">Download Report</a>
	<div class="wrap" id="valet-support-report-wrapper">
	</div>
<?php
} else {
?>
	<div class="valet-support disconnected error">
		<?php
		echo VALET_SUPPORT_CONNECT_AVAILABILITY_ERROR;
		?>
	</div>	
<?php
}
