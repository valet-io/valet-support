<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<link  rel='stylesheet' href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" type='text/css' media='all'/>
<style>
	#valet-support-table-wrapper {
		margin-right: 20px;
	}
	.tag {
		background: #DDD;
		border-radius: .618em;
		cursor: pointer;
		margin-right: .25em;
		padding: .142em .618em;
	}
</style>

<div id="wrap">
	<h1>Valet Diagnostics</h1>

	<div id="valet-support-table-wrapper">
		<table class="display">
			<thead>
				<tr>
					<th>Property</th>
					<th>Value</th>
					<th>Tags</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ( Valet_Support_Diagnostics::get_instance()->get_all() as $key => $data ) : ?>
				<tr>
					<td>
						<?php echo esc_html( $key ); ?>
					</td>
					<td>
						<?php echo esc_html( $data->value ); ?>
					</td>
					<td>
						<?php
							array_walk(
								$data->tags,
								function( $tag ) {
									echo sprintf( '<b class="tag">#%s</b>', esc_html( $tag ) );
								}
							);
						?>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>

<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
	jQuery(document).ready(function ($) {
		$('table').DataTable({
			pageLength: 100
		});

		$('table').on( 'click', '.tag', function () {
			$('#DataTables_Table_0_filter input').val( $(this).text() ).trigger('keyup');
		});
	});
</script>
