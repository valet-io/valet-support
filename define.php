<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

define( 'VALET_SUPPORT_CONNECT_AVAILABILITY_ERROR', __( 'Your site is not connected. For availability of this feature, Please connect to the maintenance site.', 'valet-support' ) );