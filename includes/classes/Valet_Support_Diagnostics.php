<?php
/**
 * Class Valet_Support_Diagnostic
 * Inspired by Migrate DB Pro's diagnostics
 *
 * @link https://github.com/deliciousbrains/wp-migrate-db/blob/master/class/Common/Error/ErrorLog.php
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Valet_Support_Diagnostics {

	/**
	 * @var bool
	 */
	static $instance = false;

	/**
	 * Holds Diagnostic info
	 *
	 * @var array
	 */
	private $diagnostic_info = array();

	/**
	 * Valet_Support_Diagnostics constructor.
	 */
	private function __construct() {
		add_action( Valet_Support::get_instance()->get_admin_menu_hook(), array( $this, 'admin_menu' ) );
	}

	/**
	 * Get singleton instance
	 *
	 * @return bool|Valet_Support_Diagnostics
	 */
	public static function get_instance() {
		if ( ! self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Register admin menu
	 */
	public function admin_menu() {
		add_submenu_page(
			Valet_Support::get_instance()->settings()->file,
			'Valet Diagnostics',
			'Diagnostics',
			'manage_options',
			'valet-diagnostics',
			array( $this, 'render_admin_page' )
		);
	}

	/**
	 * Render diagnostics page
	 */
	static function render_admin_page() {
		include sprintf( '%s/admin/diagnostics/main.php', Valet_Support::get_instance()->settings()->path_templates );
	}

	/**
	 * Assemble and get all diagnostics
	 *
	 * @return object
	 */
	public function get_all() {
		global $wpdb;

		$this->add_diagnostic( 'site_url', site_url(), 'WordPress, database' );
		$this->add_diagnostic( 'home', home_url(), 'WordPress, database' );
		$this->add_diagnostic( 'WordPress Version', get_bloginfo( 'version' ), 'WordPress' );
		$this->add_diagnostic( 'Multisite', is_multisite() ? 'true' : 'false', 'WordPress' );

		if ( is_multisite() ) {
			$this->add_diagnostic(
				'install',
				defined( 'SUBDOMAIN_INSTALL' ) && SUBDOMAIN_INSTALL ? 'Sub-domain' : 'Sub-directory',
				'WordPress, multisite'
			);

			$diagnostic_info['multisite-info'] = array(
				'Domain Current Site'  => defined( 'DOMAIN_CURRENT_SITE' ) ? DOMAIN_CURRENT_SITE : 'Not Defined',
				'Path Current Site'    => defined( 'PATH_CURRENT_SITE' ) ? PATH_CURRENT_SITE : 'Not Defined',
				'Site ID Current Site' => defined( 'SITE_ID_CURRENT_SITE' ) ? SITE_ID_CURRENT_SITE : 'Not Defined',
				'Blog ID Current Site' => defined( 'BLOG_ID_CURRENT_SITE' ) ? BLOG_ID_CURRENT_SITE : 'Not Defined',
				'Uploads Blog Dir'     => defined( 'UPLOADBLOGSDIR' ) && UPLOADBLOGSDIR ? UPLOADBLOGSDIR : 'Not defined',
			);
		}

		/**
		 * PHP, MySQL, and Server
		 */
		$this->add_diagnostic( 'Database Name', $wpdb->dbname, 'database' );
		$this->add_diagnostic( 'Table Prefix', $wpdb->base_prefix, 'database' );

		$this->add_diagnostic( 'Web Server', ! empty( $_SERVER['SERVER_SOFTWARE'] ) ? $_SERVER['SERVER_SOFTWARE'] : '', 'server' );
		$this->add_diagnostic( 'PHP', ( function_exists( 'phpversion' ) ) ? phpversion() : '', 'server' );
		$this->add_diagnostic( 'WP Memory Limit', WP_MEMORY_LIMIT, 'server' );
		$this->add_diagnostic( 'PHP Time Limit', ( function_exists( 'ini_get' ) ) ? ini_get( 'max_execution_time' ) : '', 'server' );
		$this->add_diagnostic( 'Blocked External HTTP Requests', ( ! defined( 'WP_HTTP_BLOCK_EXTERNAL' ) || ! WP_HTTP_BLOCK_EXTERNAL ) ? 'None' : ( WP_ACCESSIBLE_HOSTS ? 'Partially (Accessible Hosts: ' . WP_ACCESSIBLE_HOSTS . ')' : 'All' ), 'server' );
		$this->add_diagnostic( 'fsockopen', ( function_exists( 'fsockopen' ) ) ? 'Enabled' : 'Disabled', 'server' );
		$this->add_diagnostic( 'OpenSSL', ( $this->open_ssl_enabled() ) ? OPENSSL_VERSION_TEXT : 'Disabled', 'server' );
		$this->add_diagnostic( 'cURL', ( function_exists( 'curl_init' ) ) ? 'Enabled' : 'Disabled', 'server' );

		$this->add_diagnostic( 'MySQL', mysqli_get_server_info( $wpdb->dbh ), 'database' );
		$this->add_diagnostic( 'ext/mysqli', empty( $wpdb->use_mysqli ) ? 'no' : 'yes', 'database' );
		$this->add_diagnostic( 'WP Locale', get_locale(), 'database' );
		$this->add_diagnostic( 'DB Charset', DB_CHARSET, 'database' );

		/**
		 * WordPress specific
		 */
		$this->add_diagnostic( 'Debug Mode', ( defined( 'WP_DEBUG' ) && WP_DEBUG ) ? 'Yes' : 'No', 'debug' );
		$this->add_diagnostic( 'Debug Log', ( defined( 'WP_DEBUG_LOG' ) && WP_DEBUG_LOG ) ? 'Yes' : 'No', 'debug' );
		$this->add_diagnostic( 'Debug Display', ( defined( 'WP_DEBUG_DISPLAY' ) && WP_DEBUG_DISPLAY ) ? 'Yes' : 'No', 'debug' );
		$this->add_diagnostic( 'Script Debug', ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? 'Yes' : 'No', 'debug' );
		$this->add_diagnostic( 'PHP Error Log', ( function_exists( 'ini_get' ) ) ? ini_get( 'error_log' ) : '', 'debug' );
		$this->add_diagnostic( 'WP Max Upload Size', size_format( wp_max_upload_size() ), 'WordPress' );
		$this->add_diagnostic( 'PHP Post Max Size', size_format( $this->get_post_max_size() ), 'php, server' );

		$this->add_diagnostic( 'WP_HOME', ( defined( 'WP_HOME' ) && WP_HOME ) ? WP_HOME : 'Not defined', 'constants, WordPress' );
		$this->add_diagnostic( 'WP_SITEURL', ( defined( 'WP_SITEURL' ) && WP_SITEURL ) ? WP_SITEURL : 'Not defined', 'constants, site' );
		$this->add_diagnostic( 'WP_CONTENT_URL', ( defined( 'WP_CONTENT_URL' ) && WP_CONTENT_URL ) ? WP_CONTENT_URL : 'Not defined', 'constants, content' );
		$this->add_diagnostic( 'WP_CONTENT_DIR', ( defined( 'WP_CONTENT_DIR' ) && WP_CONTENT_DIR ) ? WP_CONTENT_DIR : 'Not defined', 'constants, content' );
		$this->add_diagnostic( 'WP_PLUGIN_DIR', ( defined( 'WP_PLUGIN_DIR' ) && WP_PLUGIN_DIR ) ? WP_PLUGIN_DIR : 'Not defined', 'constants, plugin' );
		$this->add_diagnostic( 'WP_PLUGIN_URL', ( defined( 'WP_PLUGIN_URL' ) && WP_PLUGIN_URL ) ? WP_PLUGIN_URL : 'Not defined', 'constants, plugin' );
		$this->add_diagnostic( 'UPLOADS', ( defined( 'UPLOADS' ) && UPLOADS ) ? UPLOADS : 'Not defined', 'constants' );

		$this->add_diagnostic( 'Permalink Structure', get_option( 'permalink_structure' ), 'WordPress' );

		/**
		 * Themes
		 */
		$theme_info = wp_get_theme();

		$this->add_diagnostic( 'Active Theme Name', $theme_info->Name, 'theme' );
		$this->add_diagnostic( 'Active Theme Folder', $theme_info->get_stylesheet_directory(), 'theme' );

		if ( $theme_info->get( 'Template' ) ) {
			$this->add_diagnostic( 'Parent Theme Folder', $theme_info->get( 'Template' ), 'theme' );
		}

		/**
		 * Plugins
		 *
		 * @todo include Multisite support
		 */
		$active_plugins = (array) get_option( 'active_plugins', array() );

		foreach ( $active_plugins as $plugin ) {
			$active_plugins_log[1][] = $this->get_plugin_details( WP_PLUGIN_DIR . '/' . $plugin, isset( $whitelist[ $plugin ] ) ? '*' : '' );
		}

		$active_plugins = (array) get_option( 'active_plugins', array() );

		$this->add_diagnostic( 'Active Plugins Count', count( $active_plugins ), 'plugin' );

		/**
		 * Uploads
		 */
		if ( function_exists( 'wp_upload_dir' ) ) {
			$wp_upload_dir = wp_upload_dir();
			$this->add_diagnostic( 'Upload basedir', $wp_upload_dir['basedir'], 'WordPress' );

			$is_writable = false;
			if ( isset( $wp_upload_dir['basedir'] ) && function_exists( 'is_writable' ) ) {
				$is_writable = is_writable( $wp_upload_dir['basedir'] ) ? 'true' : 'false';
			}
			$this->add_diagnostic( 'Upload is writeable', $is_writable, 'WordPress' );
		}

		/**
		 * Must Use Plugins
		 */
		$mu_plugins = wp_get_mu_plugins();

		if ( ! $mu_plugins ) {
			$mu_plugins = array();
		}

		$this->add_diagnostic( 'Must-use plugins', count( $mu_plugins ), 'plugin' );

		/**
		 * Return it all
		 */
		return (object) $this->diagnostic_info;
	}

	/**
	 * Add diagnostic to array
	 *
	 * @param $key
	 * @param $value
	 * @param array $tags
	 */
	private function add_diagnostic( $key, $value, $tags = array() ) {
		if ( is_string( $tags ) ) {
			$tags = array_filter( preg_split( '/[\s,]/', $tags ) );
		}

		$this->diagnostic_info[ $key ] = (object) array(
			'value' => $value,
			'tags'  => $tags,
		);
	}

	public function open_ssl_enabled() {
		if ( defined( 'OPENSSL_VERSION_TEXT' ) ) {
			return true;
		} else {
			return false;
		}
	}

	public function get_post_max_size() {
		$bytes = max( wp_convert_hr_to_bytes( trim( ini_get( 'post_max_size' ) ) ), wp_convert_hr_to_bytes( trim( ini_get( 'hhvm.server.max_post_size' ) ) ) );

		return $bytes;
	}

	public function get_plugin_details( $plugin_path, $prefix = '' ) {
		$plugin_data = get_plugin_data( $plugin_path );
		$plugin_name = strlen( $plugin_data['Name'] ) ? $plugin_data['Name'] : basename( $plugin_path );

		if ( empty( $plugin_name ) ) {
			return;
		}

		$version = '';
		if ( $plugin_data['Version'] ) {
			$version = sprintf( ' (v%s)', $plugin_data['Version'] );
		}

		$author = '';
		if ( $plugin_data['AuthorName'] ) {
			$author = sprintf( ' by %s', $plugin_data['AuthorName'] );
		}

		return sprintf( '%s %s%s%s', $prefix, $plugin_name, $version, $author );
	}
}
