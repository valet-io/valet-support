<?php
/**
 * Class Valet_Support_Activity_Log
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Valet_Support_Activity_Log {
	/**
	 * add all actions related to Activity Log
	 */
	public function init() {
		add_action( Valet_Support::get_instance()->get_admin_menu_hook(), array( $this, 'admin_menu' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ), 10, 1 );

	}

	public function admin_menu() {
		add_submenu_page(
			Valet_Support::get_instance()->settings()->file,
			'Activity Log',
			'Activity Log',
			'manage_options',
			'valet-activity-log',
			array( $this, 'render_admin_page' )
		);
	}

	/**
	 * Render admin page
	 */
	public function render_admin_page() {
		include sprintf( '%s/admin/activity-log/main.php', Valet_Support::get_instance()->settings()->path_templates );
	}

	/**
	 * Enqueue notes scripts
	 */
	public function admin_enqueue_scripts( $hook_suffix ) {

		// bail if easy-notes admin page doesn't
		if ( 'valet-support_page_valet-activity-log' !== $hook_suffix ) {
			return;
		}

		$asset_file = include Valet_Support::get_instance()->settings()->path . '/assets//build/valet-activity-logs/index.asset.php';

		wp_enqueue_script(
			'valet-activity-log-admin-js',
			Valet_Support::get_instance()->settings()->uri . 'assets/build/valet-activity-logs/index.js',
			$asset_file['dependencies'],
			$asset_file['version'],
			true
		);

		$connect_info = Valet_Support::get_instance()->get_connect_info();
		wp_localize_script(
			'valet-activity-log-admin-js',
			'valet_activity_logs',
			array(
				'route_url' 	 => isset( $connect_info['site_url'] ) ? $connect_info['site_url']. Valet_Support::CONNECT_PREFIX . 'activity-logs' : '',
				'connect_info' 	 => $connect_info,
			)
		);

		wp_enqueue_style( 'wp-components' );
		wp_enqueue_style(
			'valet-activity-log-admin-style',
			Valet_Support::get_instance()->settings()->uri . 'assets/build/valet-activity-logs/index.css',
			array(),
			( defined( 'WP_DEBUG' ) && WP_DEBUG ) ? time() : Valet_Support::get_instance()->settings()->plugin_data['Version'],
			'all'
		);
	}
}
