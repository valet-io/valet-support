<?php
/**
 * Class Valet_Support_Speed_Log
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Valet_Support_Speed_Log {

	public function init() {
		add_action( Valet_Support::get_instance()->get_admin_menu_hook(), array( $this, 'admin_menu' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ), 10, 1 );
	}

	public function admin_menu() {
		add_submenu_page(
			Valet_Support::get_instance()->settings()->file,
			'Speed Log',
			'Speed Log',
			'manage_options',
			'valet-speed-log',
			array( $this, 'render_admin_page' )
		);
	}

	public function render_admin_page() {
		include sprintf( '%s/admin/speed-log/main.php', Valet_Support::get_instance()->settings()->path_templates );
	}

	public function admin_enqueue_scripts( $hook_suffix ) {
		if ( 'valet-support_page_valet-speed-log' !== $hook_suffix ) {
			return;
		}

		$asset_file = include Valet_Support::get_instance()->settings()->path . '/assets/build/valet-speed-logs/index.asset.php';

		wp_enqueue_script(
			'valet-speed-logs-admin-js',
			Valet_Support::get_instance()->settings()->uri . 'assets/build/valet-speed-logs/index.js',
			$asset_file['dependencies'],
			$asset_file['version'],
			true
		);

		$connect_info = Valet_Support::get_instance()->get_connect_info();
		wp_localize_script(
			'valet-speed-logs-admin-js',
			'valet_speed_logs',
			array(
				'route_url' 	 => isset( $connect_info['site_url'] ) ? $connect_info['site_url']. Valet_Support::CONNECT_PREFIX . 'speed-logs' : '',
				'connect_info' 	 => $connect_info,
			)
		);

		wp_enqueue_style( 'wp-components' );
		wp_enqueue_style(
			'valet-speed-logs-admin-style',
			Valet_Support::get_instance()->settings()->uri . 'assets/build/valet-speed-logs/index.css',
			array(),
			( defined( 'WP_DEBUG' ) && WP_DEBUG ) ? time() : Valet_Support::get_instance()->settings()->plugin_data['Version'],
			'all'
		);
	}

}
