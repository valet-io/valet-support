<?php
/**
 * Class Valet_Support_Requirements
 *
 * Inspired by Migrate DB Pro's diagnostics
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Valet_Support_Requirements {

	/**
	 * @var bool
	 */
	static $instance = false;

	/**
	 * Plugins required by Valet Support
	 *
	 * @var array
	 */
	private $required_plugins = array(
		array(
			'name'   => 'Activity Log',
			'link'   => 'https://wordpress.org/plugins/aryo-activity-log/',
			'path'   => 'aryo-activity-log/aryo-activity-log.php',
			'active' => false,
		),
	);

	/**
	 * Valet_Support_Diagnostics constructor.
	 */
	private function __construct() {

	}

	/**
	 * Get singleton instance
	 *
	 * @return bool|Valet_Support_Diagnostics
	 */
	public static function get_instance() {
		if ( ! self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Get outside links
	 *
	 * @param array
	 */
	public function get_required_outside_links() {
		$home_url    = get_home_url();
		$home_domain = preg_replace( '/https?\:\/\/(www.)?/i', '', $home_url );

		return array(
			'google_search_console' => array(
				'url'  => 'https://search.google.com/u/3/search-console?resource_id=' . get_home_url(),
				'name' => __( 'Google Search console', 'valet-support' ),
			),
			'blogvault'             => array(
				'url'  => 'https://app.blogvault.net/app/sites/',
				'name' => __( 'Blogvault', 'valet-support' ),
			),
			'security_headers'      => array(
				'url'  => 'https://securityheaders.com/?q=' . $home_domain . '&followRedirects=on',
				'name' => __( 'Security Headers', 'valet-support' ),
			),
		);
	}
}
