<?php
/**
 * Class Valet_Support_Backup_Logger
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Valet_Support_Backup_Logger {

	private $update_theme_option_name = '_site_transient_update_themes';
	private $bv_backup_time_option_name = 'bvAccountsList';
	private $bv_key_option_name 		= 'bvApiPublic';
	private $last_backup_time_option_name = 'valet_support_last_backup_time';


	public function init() {
		if ( function_exists('update_site_option') ) {
			add_action( 'update_site_option_' . $this->update_theme_option_name, [$this, 'log_backup_time_from_update_site_option'], 1, 4 );
		} else {
			add_action( 'update_option_' . $this->update_theme_option_name, [$this, 'log_backup_time_from_update_option' ], 10, 3 );
		}
	}

	public function log_backup_time_from_update_site_option( $option, $value, $old_value, $network_id ) {
		$this->log_backup_time_from_update_option( $old_value, $value, $option );
	}

	public function log_backup_time_from_update_option( $old_value, $value, $option ) {
		if ( $option != $this->update_theme_option_name ) {
			return;
		}

		$key = $this->get_bv_account_key();
		if ( false  === $key ) {
			return;
		}

		$bv_account_list = get_site_option( $this->bv_backup_time_option_name, false );
		if ( false === $bv_account_list ) {
			return;
		}

		$last_backup_time = get_site_option( $this->last_backup_time_option_name, false );
		if ( false === $last_backup_time || ( isset( $bv_account_list[$key]['lastbackuptime'] ) && $bv_account_list[$key]['lastbackuptime'] != $last_backup_time ) ) {
			$this->add_backup_time_log( date( 'Y-m-d H:i:s', intval( $bv_account_list[$key]['lastbackuptime'] ) ) );
		}
	}

	private function get_bv_account_key() {
		$res = false;
		if ( function_exists('get_site_option') ) {
			$res = get_site_option( $this->bv_key_option_name, false );
		}
		if ( $res === false ) {
			$res = get_option( $this->bv_key_option_name, false );
		}
		return $res;
	}

	private function add_backup_time_log( $backup_date_time ) {
		$is_connected = Valet_Support::get_instance()->is_connected();
		if ( ! $is_connected ) {
			return false;
		}

		$data					  = array();
		$data['backup_date_time'] = $backup_date_time;

		$connect_info = Valet_Support::get_instance()->get_connect_info();
		$args = [
			'body' => $data,
			'headers' => [
				'Authorization' => 'Basic ' . base64_encode( $connect_info['user_login'] . ':' . $connect_info['password'] )
			],
			'timeout'     => 60,
		];
	
		$response   = wp_remote_post( $connect_info['site_url']. Valet_Support::CONNECT_PREFIX . 'backup-logs', $args );
		$http_code  = wp_remote_retrieve_response_code( $response );
	
		$ret		= wp_remote_retrieve_body( $response );

		unset( $data );
		unset( $connect_info );
	}
}
