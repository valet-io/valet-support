<?php
/**
 * Class Valet_Support_Speed_Log
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Valet_Support_Speed_Logger {

	public function init() {
		add_action( 'valet_daily_speed_check', array( $this, 'valet_daily_speed_check' ) );
	}

	public function valet_daily_speed_check() {
		$is_connected = Valet_Support::get_instance()->is_connected();
		if ( ! $is_connected ) {
			return false;
		}

		set_time_limit(0);
		$is_multisite = is_multisite();

		if ( $is_multisite ) {
			$current_site_id = get_current_blog_id();
			error_log(
				sprintf( 'valet_daily_speed_check ran! %s',  $current_site_id
				)
			);
			$url = get_home_url( $current_site_id );
		} else {
			$url = get_home_url();
		}
		// Temp

		// error_log( 'Guarded URL: ' . $url );
		// $url = 'https://www.valet.io/';

		// Temp
		// if ( false  === ( $json_response = get_transient( 'valet_speed_data' ) ) ) {
			$device = 'desktop';
			$request = add_query_arg( array(
							'url'=> $url,
							'category' => 'performance',	
							'strategy' => $device,							
							), 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed' );
			$args = array( 'timeout' => 60 );
			$response = wp_safe_remote_get( $request, $args );
			if ( is_wp_error( $response) ) {
				return false;
			}
			$response = wp_remote_retrieve_body($response);	
			$json_response = json_decode($response, true, 1512);

			// Temp
			// set_transient( 'valet_speed_data' , $json_response, 0 );
		// }
		
		
		if ( ! empty($json_response['lighthouseResult'] ) ) {
			/*
			$largest_contentful_paint = $json_response['lighthouseResult']['audits']['largest-contentful-paint']; //title, description, score, scoreDisplayMode, displayValue, numericValue
			$first_contentful_paint = $json_response['lighthouseResult']['audits']['first-contentful-paint']; //title, description, score, scoreDisplayMode, displayValue, numericValue
			$first_meaningful_paint = $json_response['lighthouseResult']['audits']['first-meaningful-paint']; //title, description, score, scoreDisplayMode, displayValue, numericValue 
			*/
			$speed_index = $json_response['lighthouseResult']['audits']['speed-index']; //title, description, score, scoreDisplayMode, displayValue, numericValue 

			$data['speed_index'] 	  = $speed_index;
			$data['requested_url']	  = $json_response['lighthouseResult']['requestedUrl'];
			$data['other_speed_data'] = $json_response['lighthouseResult'];
			$data['is_multisite'] 	  = $is_multisite ? 1 : 0;

			if ( $is_multisite ) {
				$current_site_id = get_current_blog_id();
				$data['site_id'] = $current_site_id;

				$current_site_details = get_blog_details( array( 'blog_id' => $current_site_id ) );
				$data['site_name']    = $current_site_details->blogname;
			} else {
				$data['site_id']   = 0;
				$data['site_name'] = '';
			}

			$data['create_date'] = current_time( 'mysql', true );

			$connect_info = Valet_Support::get_instance()->get_connect_info();
			$args = [
				'body' => $data,
				'headers' => [
					'Authorization' => 'Basic ' . base64_encode( $connect_info['user_login'] . ':' . $connect_info['password'] )
				],
				'timeout'     => 60,
			];
		
			$response   = wp_remote_post( $connect_info['site_url']. Valet_Support::CONNECT_PREFIX . 'speed-logs', $args );
			$http_code = wp_remote_retrieve_response_code( $response );
		
			$ret = wp_remote_retrieve_body( $response );

			unset( $is_multisite );
			unset( $data );
			unset( $args );
			unset( $connect_info );
		} else {
			//TODO error handling
			echo "<pre>";
			print_r( $json_response );
			echo "</pre>";
		}
	}
}
