<?php
/**
 * Class Valet_Support_Report
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Valet_Support_Report {

	public function init() {
		add_action( Valet_Support::get_instance()->get_admin_menu_hook(), array( $this, 'admin_menu' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ), 10, 1 );
	}

	public function admin_menu() {
		add_submenu_page(
			Valet_Support::get_instance()->settings()->file,
			'Report',
			'Report',
			'manage_options',
			'valet-report',
			array( $this, 'render_admin_page' )
		);
	}

	public function render_admin_page() {
		include sprintf( '%s/admin/report/main.php', Valet_Support::get_instance()->settings()->path_templates );
	}

	public function admin_enqueue_scripts( $hook_suffix ) {
		if ( 'valet-support_page_valet-report' !== $hook_suffix ) {
			return;
		}

		$asset_file = include Valet_Support::get_instance()->settings()->path . '/assets/build/valet-report/index.asset.php';

		wp_enqueue_script(
			'valet-report-admin-js',
			Valet_Support::get_instance()->settings()->uri . 'assets/build/valet-report/index.js',
			$asset_file['dependencies'],
			$asset_file['version'],
			true
		);

		$connect_info = Valet_Support::get_instance()->get_connect_info();

		$is_multisite = is_multisite();
		if ( $is_multisite ) {
			switch_to_blog( get_main_site_id() );
		}
		$gmt_offset = get_option('gmt_offset');
		$timezone_string = wp_timezone_string();
		if ( $is_multisite ) {
			restore_current_blog();
		}

		$post_count     = wp_count_posts();
		$comment_count  = wp_count_comments();
		$user_count 	= count_users();
		wp_localize_script(
			'valet-report-admin-js',
			'valet_report',
			array(
				'route_url' 	  		  		=> isset($connect_info['site_url']) ? $connect_info['site_url'] . Valet_Support::CONNECT_PREFIX . 'report' : '',
				'connect_info'			  		=> $connect_info,
				'gmt_offset'			  		=> get_option('gmt_offset'),
				'timezone_string'		  		=> wp_timezone_string(),
				'installed_plugins_count' 		=> count(get_plugins()),
				'installed_themes_count'  		=> count(wp_get_themes()),
				'wp_version' 			  		=> $GLOBALS['wp_version'],
				'posts_count'			  		=> $post_count->publish,
				'comments_count'		  		=> $comment_count->approved,
				'users_count'			  		=> $user_count['total_users'],
				'administrator_role_user_count' => $user_count['avail_roles']['administrator']
			)
		);

		wp_enqueue_style( 'wp-components' );
		wp_enqueue_style(
			'valet-report-admin-style',
			Valet_Support::get_instance()->settings()->uri . 'assets/build/valet-report/index.css',
			array(),
			( defined( 'WP_DEBUG' ) && WP_DEBUG ) ? time() : Valet_Support::get_instance()->settings()->plugin_data['Version'],
			'all'
		);
	}

}
