<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Valet_Support_Activity_Logger {

	/**
	 * initialize logger
	 *
	 * @return void
	 */
	public function init() {
		$is_connected = Valet_Support::get_instance()->is_connected();
		if ( ! $is_connected ) {
			return false;
		}

		add_action( '_core_updated_successfully', array( $this, 'core_updated_successfully' ) );

		add_action( 'activated_plugin', array( $this, 'activated_deactivated_plugin' ), 10, 2 );
		add_action( 'deactivated_plugin', array( $this, 'activated_deactivated_plugin' ), 10, 2 );

		add_action( 'upgrader_process_complete', array( $this, 'upgrader_process_complete' ), 10, 2 );
		add_action( 'deleted_plugin', array( $this, 'deleted_plugin' ), 10, 2 );

		add_action( 'update_site_option_allowedthemes', array( $this, 'network_enable_disable_theme' ), 10 , 3 );
		add_action( 'switch_theme', array( $this, 'switch_theme' ), 10, 3 );
		add_action( 'delete_site_transient_update_themes', array( $this, 'delete_site_transient_update_themes' ) );

		// User activity
		if ( is_multisite() ) { // For Network
			// do_action( 'after_signup_user', $user, $user_email, $key, $meta );
			add_action( 'after_signup_user', array( $this, 'after_signup_user' ), 10, 4 );
			// do_action( 'wpmu_activate_user', $user_id, $password, $meta );
			add_action( 'wpmu_activate_user', array( $this, 'wpmu_activate_user' ), 10, 3 );

			add_action( 'wpmu_new_user', array( $this, 'wpmu_new_user' ), 10, 1 );
			add_action( 'wpmu_delete_user', array( $this, 'wpmu_delete_user' ), 10, 2 );
			foreach ( [ 'granted_super_admin', 'revoked_super_admin' ] as $action_hook ) {
				add_action( $action_hook, array( $this, 'granted_revoked_super_admin' ), 10, 1 );
			}
			add_action( 'add_user_to_blog', array( $this, 'add_user_to_blog' ), 1, 3 );
			// add_action( 'invite_user', array( $this, 'invite_user' ), 10, 3 );
			add_action( 'remove_user_from_blog', array( $this, 'remove_user_from_blog' ), 10, 3 );
		} else {
			add_action( 'user_register', array( $this, 'user_register' ), 10, 2 );
			add_action( 'delete_user', array( $this, 'delete_user' ), 10, 3 );
		}
	}

	/**
	 * core update log
	 *
	 * @param string $wp_version
	 * @return void
	 */
	public function core_updated_successfully( $wp_version ) {
		global $pagenow;

		// Auto updated
		if ( 'update-core.php' !== $pagenow ) {
			$action = 'auto-update';
		} else {
			$action = 'update';
		}

		$prev_wp_core_versions = Valet_Support::get_instance()->get_wp_core_version();

		$this->_log(
			array(
				'action'              => $action,
				'object_type'         => 'wp-core',
				'object_slug'         => 'WordPress',
				'object_name'         => 'WordPress Core',
				'object_subtype_from' => $prev_wp_core_versions,
				'object_subtype_to'   => $wp_version,
			),
			array(
				'%s', // action
				'%s', // object_type
				'%s', // object_slug
				'%s', // object_name
				'%s', // object_subtype_from
				'%s', // object_subtype_to
			)
		);

		Valet_Support::get_instance()->save_wp_core_version();
	}

	/**
	 * handle plugin/theme update event
	 *
	 * @param WP_Upgrader $upgrader
	 * @param array       $args Array of arguments related to asset updated.
	 */
	public function upgrader_process_complete( $upgrader, $args ) {
		if ( ! isset( $args['type'] ) ) {
			return;
		}

		if ( 'plugin' === $args['type'] ) {
			if ( 'install' === $args['action'] ) {
				$this->_install_plugin( $upgrader );
			} elseif ( 'update' === $args['action'] ) {
				$this->_update_plugin( $upgrader, $args );
			}
		} elseif ( 'theme' === $args['type'] ) {
			if ( 'install' === $args['action'] ) {
				$this->_install_theme( $upgrader );
			} elseif ( 'update' === $args['action'] ) {
				$this->_update_theme( $upgrader, $args );
			}
		}
	}

	/**
	 * install plugin
	 *
	 * @param object $upgrader
	 * @return void
	 */
	private function _install_plugin( $upgrader ) {
		$plugin_slug = $upgrader->plugin_info();
		if ( ! $plugin_slug ) {
			return;
		}

		$plugin_data = get_plugin_data( $upgrader->skin->result['local_destination'] . '/' . $plugin_slug, true, false );

		$this->_log(
			array(
				'action'            => 'install',
				'object_type'       => 'plugin',
				'object_slug'       => $plugin_slug,
				'object_name'       => $plugin_data['Name'],
				'object_subtype_to' => $plugin_data['Version'],
			),
			array(
				'%s', // action
				'%s', // object_type
				'%s', // object_slug
				'%s', // object_name
				'%s', // object_subtype_to
			)
		);

		Valet_Support::get_instance()->save_plugins_versions();
	}

	/**
	 * update plugin
	 *
	 * @param object $upgrader
	 * @param array  $args
	 * @return void
	 */
	private function _update_plugin( $upgrader, $args ) {
		if ( isset( $args['bulk'] ) && true == $args['bulk'] ) {
			$slugs = (array) $args['plugins'];
		} else {
			if ( ! isset( $upgrader->skin->plugin ) ) {
				return;
			}

			$slugs = array( $upgrader->skin->plugin );
		}

		$prev_plugins_versions = Valet_Support::get_instance()->get_plugins_versions();

		foreach ( $slugs as $slug ) {
			$plugin_data = $this->_get_plugin_data( $slug );

			$this->_log(
				array(
					'action'              => 'update',
					'object_type'         => 'plugin',
					'object_slug'         => $slug,
					'object_name'         => $plugin_data['Name'],
					'object_subtype_from' => $prev_plugins_versions[ $slug ]['version'],
					'object_subtype_to'   => $plugin_data['Version'],
				),
				array(
					'%s', // action
					'%s', // object_type
					'%s', // object_slug
					'%s', // object_name
					'%s', // object_subtype_from
					'%s', // object_subtype_to
				)
			);
		}

		Valet_Support::get_instance()->save_plugins_versions();
	}

	/**
	 * activate deactivate plugin
	 *
	 * @param string $plugin_slug
	 * @param bool   $network_wide
	 * @return void
	 */
	public function activated_deactivated_plugin( $plugin_slug, $network_wide ) {
		$plugin_data    = $this->_get_plugin_data( $plugin_slug );
		$current_action = current_action();
		switch ( $current_action ) {
			case 'activated_plugin':
				$action = $network_wide ? 'network-activate' : 'activate';
				break;
			case 'deactivated_plugin':
				$action = $network_wide ? 'network-deactivate' : 'deactivate';
				break;
			default:
				throw new Exception( 'Invalid current_action() val: ' . $current_action );
		}

		$this->_log(
			array(
				'action'            => $action,
				'object_type'       => 'plugin',
				'object_slug'       => $plugin_slug,
				'object_name'       => $plugin_data['Name'],
				'object_subtype_to' => $plugin_data['Version'],
			),
			array(
				'%s', // action
				'%s', // object_type
				'%s', // object_slug
				'%s', // object_name
				'%s', // object_subtype_to
			)
		);
	}

	/**
	 * delete plugin
	 *
	 * @param string  $plugin_slug
	 * @param boolean $deleted
	 * @return void
	 */
	public function deleted_plugin( $plugin_slug, $deleted = false ) {
		$prev_plugins_versions = Valet_Support::get_instance()->get_plugins_versions();

		$this->_log(
			array(
				'action'            => 'uninstall',
				'object_type'       => 'plugin',
				'object_slug'       => $plugin_slug,
				'object_name'       => $prev_plugins_versions[ $plugin_slug ]['name'],
				'object_subtype_to' => $prev_plugins_versions[ $plugin_slug ]['version'],
			),
			array(
				'%s', // action
				'%s', // object_type
				'%s', // object_slug
				'%s', // object_name
				'%s', // object_subtype_to
			)
		);

		Valet_Support::get_instance()->save_plugins_versions();
	}

	/**
	 * install theme
	 *
	 * @param object $upgrader
	 * @return void
	 */
	private function _install_theme( $upgrader ) {
		$upgrader_theme_object = $upgrader->theme_info();
		if ( ! $upgrader_theme_object ) {
			return;
		}

		wp_clean_themes_cache();

		$temp_theme_object = wp_get_theme( $upgrader_theme_object );
		$name              = $upgrader_theme_object->name;
		$version           = $temp_theme_object->stylesheet->version;
		$theme_slug        = $temp_theme_object->stylesheet->stylesheet;

		$this->_log(
			array(
				'action'            => 'install',
				'object_type'       => 'theme',
				'object_slug'       => $theme_slug,
				'object_name'       => $name,
				'object_subtype_to' => $version,
			),
			array(
				'%s', // action
				'%s', // object_type
				'%s', // object_slug
				'%s', // object_name
				'%s', // object_subtype_to
			)
		);
		Valet_Support::get_instance()->save_themes_versions();
	}

	private function _update_theme( $upgrader, $args ) {
		if ( isset( $args['bulk'] ) && true == $args['bulk'] ) {
			$theme_slugs = $args['themes'];
		} else {
			$theme_slugs = array( $upgrader->skin->theme );
		}

		$prev_themes_versions = Valet_Support::get_instance()->get_themes_versions();

		foreach ( $theme_slugs as $theme_slug ) {
			$theme = wp_get_theme( $theme_slug );

			$name    = $theme->Name;
			$version = $theme->Version;

			$this->_log(
				array(
					'action'              => 'update',
					'object_type'         => 'theme',
					'object_slug'         => $theme_slug,
					'object_name'         => $name,
					'object_subtype_from' => $prev_themes_versions[ $theme_slug ]['version'],
					'object_subtype_to'   => $version,
				),
				array(
					'%s', // action
					'%s', // object_type
					'%s', // object_slug
					'%s', // object_name
					'%s', // object_subtype_from
					'%s', // object_subtype_to
				)
			);
			Valet_Support::get_instance()->save_themes_versions();
		}
	}

	/**
	 * network enable disable theme
	 *
	 * @param string $option
	 * @param mixed  $new_theme
	 * @param mixed  $old_theme
	 * @return void
	 */
	public function network_enable_disable_theme(  $option, $value, $old_value ) {
		if ( 'allowedthemes' !== $option ) {
			return;
		}

		$new_themes = array_keys( $value );
		$old_themes = array_keys( $old_value );

		$disabled_themes = array_diff( $old_themes, $new_themes );
		$enabled_themes = array_diff( $new_themes, $old_themes );
		$themes_versions = Valet_Support::get_instance()->get_themes_versions();

		foreach ( $enabled_themes as $enabled_theme ) {
			$this->_log(
				array(
					'action'              => 'network-activate',
					'object_type'         => 'theme',
					'object_slug'         => $enabled_theme,
					'object_name'         => $themes_versions[$enabled_theme]['name'],
					'object_subtype_to'   => $themes_versions[$enabled_theme]['version'],
				),
				array(
					'%s', // action
					'%s', // object_type
					'%s', // object_slug
					'%s', // object_name
					'%s', // object_subtype_to
				)
			);
		}

		foreach ( $disabled_themes as $enabled_theme ) {
			$this->_log(
				array(
					'action'              => 'network-deactivate',
					'object_type'         => 'theme',
					'object_slug'         => $enabled_theme,
					'object_name'         => $themes_versions[$enabled_theme]['name'],
					'object_subtype_to'   => $themes_versions[$enabled_theme]['version'],
				),
				array(
					'%s', // action
					'%s', // object_type
					'%s', // object_slug
					'%s', // object_name
					'%s', // object_subtype_to
				)
			);
		}

		Valet_Support::get_instance()->save_themes_versions();
	}

	/**
	 * switch theme
	 *
	 * @param string   $new_name
	 * @param WP_Theme $new_theme
	 * @param WP_Theme $old_theme
	 * @return void
	 */
	public function switch_theme( $new_name, WP_Theme $new_theme, WP_Theme $old_theme ) {
		$this->_log(
			array(
				'action'              => 'switch_theme',
				'object_type'         => 'theme',
				'object_slug'         => $new_theme->get_stylesheet(),
				'object_name'         => $new_name,
				'object_subtype_from' => $old_theme->stylesheet . ' ' . $old_theme->version,
				'object_subtype_to'   => $new_theme->stylesheet . ' ' . $new_theme->version,
			),
			array(
				'%s', // action
				'%s', // object_type
				'%s', // object_slug
				'%s', // object_name
				'%s', // object_subtype_from
				'%s', // object_subtype_to
			)
		);
		Valet_Support::get_instance()->save_themes_versions();
	}

	/**
	 * Update theme
	 *
	 * @return void
	 */
	public function delete_site_transient_update_themes() {
		$debug_backtrace = debug_backtrace();

		$delete_theme_call = null;
		foreach ( $debug_backtrace as $call ) {
			if ( isset( $call['function'] ) && 'delete_theme' === $call['function'] ) {
				$delete_theme_call = $call;
				break;
			}
		}

		if ( empty( $delete_theme_call ) ) {
			return;
		}

		$theme_slug = $delete_theme_call['args'][0];

		$prev_themes_versions = Valet_Support::get_instance()->get_themes_versions();
		$this->_log(
			array(
				'action'            => 'delete',
				'object_type'       => 'theme',
				'object_slug'       => $theme_slug,
				'object_name'       => $prev_themes_versions[ $theme_slug ]['name'],
				'object_subtype_to' => $prev_themes_versions[ $theme_slug ]['version'],
			),
			array(
				'%s', // action
				'%s', // object_type
				'%s', // object_slug
				'%s', // object_name
				'%s', // object_subtype_to
			)
		);
		Valet_Support::get_instance()->save_themes_versions();
	}

	/**
	 * user register
	 *
	 * @param integer $user_id
	 * @return void
	 */
	public function user_register( $user_id, $userdata = array() ) {
		$user = $this->_get_user_data( $user_id );
		$this->_log(
			array(
				'action'      => 'create',
				'object_type' => 'user',
				'object_slug' => $user_id,
				'object_name' => $this->get_user_object_name_from_user_object( $user ),
			),
			array(
				'%s', // action
				'%s', // object_type
				'%s', // object_slug
				'%s', // object_name
			)
		);
	}

	/**
	 * delete user
	 *
	 * @param integer      $user_id
	 * @param integer|null $reassign_user_id
	 * @param WP_User      $user
	 * @return void
	 */
	public function delete_user( $user_id, $reassign_user_id, $user ) {
		$user   = $this->_get_user_data( $user_id );
		$data   = array(
			'action'      => 'delete',
			'object_type' => 'user',
			'object_slug' => $user_id,
			'object_name' => $this->get_user_object_name_from_user_object( $user ),
		);
		$format = array(
			'%s', // action
			'%s', // object_type
			'%s', // object_slug
			'%s', // object_name
		);
		if ( ! is_null( $reassign_user_id ) ) {
			$reassign_user             = $this->_get_user_data( $reassign_user_id );
			$data['object_subtype_to'] = '<a href="' . get_edit_user_link( $reassign_user_id ) . '" target="_blank">' . $reassign_user->display_name . ' [User ID: ' . $reassign_user_id . '] [' . $reassign_user->user_email . '] (role: ' . implode( ', ', $reassign_user->roles ) . ') </a>';
			$format[]                  = '%s'; // object_subtype_to
		}
		$this->_log(
			$data,
			$format
		);
	}

	public function after_signup_user( $user_login, $user_email, $key, $meta ) {
		if ( $this->is_adding_user_to_subsite() ) {
			return false;
		}

		// $meta
		/*
		Array
		(
			[add_to_blog] => 2
			[new_role] => subscriber
		)
		*/

		$sql = $GLOBALS['wpdb']->prepare( 'SELECT signup_id FROM ' . $GLOBALS['wpdb']->signups . ' WHERE  user_login=%s;', $user_login );
		$signup_id = $GLOBALS['wpdb']->get_var( $sql );
		
		$blog_id = $meta['add_to_blog'];
		
		$this->_log(
			array(
				'action'      => 'wpmu-signup-user',
				'object_type' => 'user',
				'object_slug' => $signup_id,
				'object_name' => $user_login . ' [' . $user_email . '] (role: ' . $meta['new_role'] . ')',
			),
			array(
				'%s', // action
				'%s', // object_type
				'%s', // object_slug
				'%s', // object_name
			),
			$blog_id
		);
	}

	public function  wpmu_activate_user( $user_id, $password, $meta ) {
		if ( $this->is_adding_user_to_subsite() ) {
			return false;
		}

		$user = $this->_get_user_data( $user_id );
		
		$blog_id = $meta['add_to_blog'];

		$this->_log(
			array(
				'action'      => 'wpmu-activate-user',
				'object_type' => 'user',
				'object_slug' => $user_id,
				'object_name' => $this->get_user_object_name_from_user_object( $user ),
			),
			array(
				'%s', // action
				'%s', // object_type
				'%s', // object_slug
				'%s', // object_name
			),
			$blog_id
		);
	}

	/**
	 * Fires immediately after a new user is created in Network.
	 *
	 * @param int $user_id User ID.
	 * @return void
	 */
	public function wpmu_new_user( $user_id ) {
		if ( $this->is_activating_user() ) {
			return false;
		}

		$user = $this->_get_user_data( $user_id );
		$this->_log(
			array(
				'action'      => 'wpmu-new-user',
				'object_type' => 'user',
				'object_slug' => $user_id,
				// I can't get role in this action hook.
				'object_name' => $this->get_user_object_name_from_user_object( $user ),
			),
			array(
				'%s', // action
				'%s', // object_type
				'%s', // object_slug
				'%s', // object_name
			)
		);
	}

	/**
     * Fires before a user is deleted from the network.
     *
     * @param int     $id   ID of the user about to be deleted from the network.
     * @param WP_User $user WP_User object of the user about to be deleted from the network.
	 * @return void
     */
    public function wpmu_delete_user( $user_id, $user ) {
		$user = $this->_get_user_data( $user_id );
		$data   = array(
			'action'      => 'wpmu-delete-user',
			'object_type' => 'user',
			'object_slug' => $user_id,
			'object_name' => $this->get_user_object_name_from_user_object( $user ),
		);
		$format = array(
			'%s', // action
			'%s', // object_type
			'%s', // object_slug
			'%s', // object_name
		);
		$this->_log(
			$data,
			$format
		);
	}

	public function granted_revoked_super_admin( $user_id ) {
		$data   = array(
			'action'      => str_replace( '_', '-', current_filter() ),
			'object_type' => 'user',
			'object_slug' => $user_id,
			'object_name' => $this->get_user_object_name_from_user_object( $this->_get_user_data( $user_id ) ),
		);
		$format = array(
			'%s', // action
			'%s', // object_type
			'%s', // object_slug
			'%s', // object_name
		);
		$this->_log(
			$data,
			$format
		);	
	}

	/**
	 * add user to blog
	 *
	 * @param int $user_id
	 * @param string $role
	 * @param int $blog_id
	 * @return void
	 */
    public function add_user_to_blog(  $user_id, $role, $blog_id ) {
		if ( $this->is_activating_user() ) {
			return false;
		}

		// Preventing logged the activity removing from other network sites.
		remove_action( 'wpmu_new_user', array( $this, 'wpmu_new_user' ), 10 );
		remove_action( 'remove_user_from_blog', array( $this, 'remove_user_from_blog' ), 10);

		$user = $this->_get_user_data( $user_id );
        $this->_log(
            array(
                'action'      => 'add-user-to-blog',
                'object_type' => 'user',
                'object_slug' => $user_id,
                'object_name' => '<a href="' . get_edit_user_link( $user_id ) . '" target="_blank">' . $user->display_name . ' [' . $user->user_email . '] (role: ' . $role . ') </a>',			 
            ),
            array(
                '%s', // action
                '%s', // object_type
                '%s', // object_slug
                '%s', // object_name
			),
			$blog_id
        );
    }

	/**
	 * Fires before a user is removed from a site.
	 *
	 * @param int $user_id  ID of the user being removed.
	 * @param int $blog_id  ID of the blog the user is being removed from.
	 * @param int $reassign_user_id ID of the user to whom to reassign posts.
	 * @return void
	 */
	public function remove_user_from_blog( $user_id, $blog_id, $reassign_user_id ) {
		if ( $this->is_activating_user() || $this->is_adding_user_to_subsite() ) {
			return false;
		}

		$user = $this->_get_user_data( $user_id );

		$data = array(
					'action'      => 'remove-user-from-blog',
					'object_type' => 'user',
					'object_slug' => $user_id,
					'object_name' => '<a href="' . get_edit_user_link( $user_id ) . '" target="_blank">' . $user->display_name . ' [' . $user->user_email . '] (role: ' . implode( ',', $user->roles ) . ') </a>',
				);
		$format = array(
					'%s', // action
					'%s', // object_type
					'%s', // object_slug
					'%s', // object_name
				);
		if ( intval( $reassign_user_id ) > 0 ) {
			$reassign_user             = $this->_get_user_data( intval( $reassign_user_id ) );
			$data['object_subtype_to'] = '<a href="' . get_edit_user_link( $reassign_user_id ) . '" target="_blank">' . $reassign_user->display_name . ' [User ID: ' . $reassign_user_id . '] [' . $reassign_user->user_email . '] (role: ' . implode( ', ', $reassign_user->roles ) . ') </a>';
			$format[]                  = '%s'; // object_subtype_to
		}
        $this->_log(
            $data,
            $format,
			$blog_id
        );
	}

	private function is_adding_user_to_subsite() {
		return ( isset( $_POST['noconfirmation'] ) && current_user_can( 'manage_network_users' )  );
	}

	private function is_activating_user() {
		return defined( 'WP_INSTALLING' ) && WP_INSTALLING;
	}

	/**
	 * get user data
	 *
	 * @param integer $user_id
	 * @return void
	 */
	private function _get_user_data( $user_id ) {
		return get_userdata( $user_id );
	}

	private function get_user_object_name_from_user_object( $user ) {
		$role_str = ''; 
		if ( is_array( $user->user_roles ) && count( $user->user_roles ) > 0 ) {
			$role_str = '(role: ' . implode( ', ', $user->user_roles ) . ')'; 
		}
	
		return '<a href="' . get_edit_user_link( $user->ID ) . '" target="_blank">' . $user->display_name . ' [' . $user->user_email . '] ' . $role_str . '</a>';
	}

	/**
	 * get plugin data
	 *
	 * @param string $plugin_slug
	 * @return void
	 */
	private function _get_plugin_data( $plugin_slug ) {
		if ( ! function_exists( 'get_plugins' ) ) {
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}
		return get_plugin_data( WP_PLUGIN_DIR . '/' . $plugin_slug, false, false );
	}

	private function _log( $data = array(), $format = array(), $site_id = 0 ) {
		$is_connected = Valet_Support::get_instance()->is_connected();
		if ( ! $is_connected ) {
			return false;
		}

		$is_multisite = is_multisite();

		// Action by info
		$data['user_id_of_action_by'] = get_current_user_id();
		if ( intval( get_current_user_id() ) > 0 ) {
			$current_user 					   = wp_get_current_user();
			$data['display_name_of_action_by'] = $current_user->display_name;
			$data['email_address_of_action_by'] = $current_user->user_email;
		}
		

		$data['is_multisite'] = $is_multisite ? 1 : 0;
		$format[]             = '%d';

		if ( $is_multisite ) {
			if ( $site_id > 0) {
				$current_site_id = intval( $site_id );
			} else {
				$current_site_id = get_current_blog_id();
			}
			$data['site_id'] = $current_site_id;

			$current_site_details = get_blog_details( array( 'blog_id' => $current_site_id ) );
			$data['site_name']    = $current_site_details->blogname;
		} else {
			$data['site_id']   = 0;
			$data['site_name'] = '';
		}
		$format[] = '%d';
		$format[] = '%s';

		$data['ip'] = $this->_get_ip();
		$format[]   = '%s';

		$data['create_date'] = current_time( 'mysql', true );
		$format[]            = '%s';

		$connect_info = Valet_Support::get_instance()->get_connect_info();

		// To remove Valet Support Deactivation Plugin warnings
		if ( ! isset( $connect_info['user_login'] ) || ! isset( $connect_info['password'] ) ) {
			return false;
		}

		$args = [
			'body' => $data,
			'headers' => [
				'Authorization' => 'Basic ' . base64_encode( $connect_info['user_login'] . ':' . $connect_info['password'] )
			],
			'timeout'     => 60,
		];
	
		$response = wp_remote_post( $connect_info['site_url']. Valet_Support::CONNECT_PREFIX . 'activity-logs', $args );
		$http_code = wp_remote_retrieve_response_code( $response );
	
		$ret = wp_remote_retrieve_body( $response );

		unset( $is_multisite );
		unset( $data );
		unset( $args );
		unset( $connect_info );
	}

	/**
	 * Get real IP address.
	 *
	 * @return string real address IP
	 */
	private function _get_ip() {
		$server_ip_keys = array(
			'HTTP_CF_CONNECTING_IP', // CloudFlare
			'HTTP_TRUE_CLIENT_IP', // CloudFlare Enterprise header
			'HTTP_CLIENT_IP',
			'HTTP_X_FORWARDED_FOR',
			'HTTP_X_FORWARDED',
			'HTTP_X_CLUSTER_CLIENT_IP',
			'HTTP_FORWARDED_FOR',
			'HTTP_FORWARDED',
			'REMOTE_ADDR',
		);

		$ip = '';
		foreach ( $server_ip_keys as $key ) {
			if ( isset( $_SERVER[ $key ] ) && filter_var( $_SERVER[ $key ], FILTER_VALIDATE_IP ) ) {
				$ip = trim( $_SERVER[ $key ] );
			}
		}

		if ( '::1' != $ip ) {
			return $ip;
		}

		// Fallback local ip.
		return '127.0.0.1';
	}
}
