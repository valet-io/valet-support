<?php
/**
 * Class Valet_Support_Notes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Valet_Support_Notes {

	/**
	 * @var bool
	 */
	public static $instance = false;

	/**
	 * Valet_Support_Contact constructor.
	 */
	private function __construct() {
		add_action( Valet_Support::get_instance()->get_admin_menu_hook(), array( $this, 'admin_menu' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ), 10, 1 );
	}

	/**
	 * Get Singleton Instance
	 *
	 * @return bool|Valet_Support_Contact
	 */
	public static function get_instance() {
		if ( ! self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function admin_menu() {
		add_submenu_page(
			Valet_Support::get_instance()->settings()->file,
			'Notes',
			'Notes',
			'manage_options',
			'valet-notes',
			array( $this, 'render_admin_page' )
		);
	}

	/**
	 * Render admin page
	 */
	public function render_admin_page() {
		include sprintf( '%s/admin/notes/main.php', Valet_Support::get_instance()->settings()->path_templates );
	}

	/**
	 * Enqueue notes scripts
	 */
	public function admin_enqueue_scripts( $hook_suffix ) {
		// bail if easy-notes admin page doesn't
		if ( 'valet-support_page_valet-notes' !== $hook_suffix ) {
			return;
		}

		$asset_file = include Valet_Support::get_instance()->settings()->path . '/assets/build/valet-notes/index.asset.php';

		wp_enqueue_script(
			'valet-notes-admin-js',
			Valet_Support::get_instance()->settings()->uri . 'assets/build/valet-notes/index.js',
			$asset_file['dependencies'],
			$asset_file['version'],
			true
		);

		$connect_info = Valet_Support::get_instance()->get_connect_info();
		wp_localize_script(
			'valet-notes-admin-js',
			'valet_notes',
			array(
				'route_url' 	 => isset( $connect_info['site_url'] ) ? $connect_info['site_url']. Valet_Support::CONNECT_PREFIX . 'notes' : '',
				'connect_info' 	 => $connect_info,
			)
		);

		wp_enqueue_style( 'wp-components' );
		wp_enqueue_style(
			'valet-notes-admin-style',
			Valet_Support::get_instance()->settings()->uri . 'assets/build/valet-notes/index.css',
			array(),
			( defined( 'WP_DEBUG' ) && WP_DEBUG ) ? time() : Valet_Support::get_instance()->settings()->plugin_data['Version'],
			'all'
		);
	}

}
