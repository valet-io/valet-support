<?php
/**
 * Class Valet_API
 *
 * Provides technical and diagnostic data to Valet
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Valet_Support_API {

	/**
	 * @var bool
	 */
	static $instance = false;

	/**
	 * Valet_Support_API constructor.
	 */
	private function __construct() {
		add_action( 'rest_api_init', array( $this, 'register_api_endpoints' ) );
	}

	/**
	 * Get Singleton Instance
	 *
	 * @return bool|Valet_Support_API
	 */
	public static function get_instance() {
		if ( ! self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Register Endpoints
	 */
	public function register_api_endpoints() {
		register_rest_route(
			'valetsupport/v1',
			'/versions',
			array(
				'methods'             => 'GET',
				'callback'            => array( $this, 'generate_versions_api_response' ),
				'permission_callback' => Valet_Support::get_instance()->has_capability(),
			)
		);

		register_rest_route(
			'valetsupport/v1',
			'/diagnostics',
			array(
				'methods'             => 'GET',
				'callback'            => array( $this, 'generate_diagnostics_api_response' ),
				'permission_callback' => Valet_Support::get_instance()->has_capability(),
			)
		);
	}

	/**
	 * Core, Plugins, Themes versions response
	 *
	 * @param WP_REST_Request $request
	 *
	 * @return WP_REST_Response
	 */
	public function generate_versions_api_response( WP_REST_Request $request ) {
		global $wp_version;

		$themes = array_map(
			function( $theme ) {
				return array(
					'Name'      => $theme->get( 'Name' ),
					'Author'    => $theme->get( 'Author' ),
					'AuthorURI' => $theme->get( 'AuthorURI' ),
					'Version'   => $theme->get( 'Version' ),
				);
			},
			wp_get_themes()
		);

		$data = array(
			'core'       => $wp_version,
			'themes'     => $themes,
			'plugins'    => get_plugins(),
			'mu_plugins' => get_mu_plugins(),
		);

		return new WP_REST_Response( $data );
	}

	/**
	 * Diagnostics Response
	 *
	 * @return WP_REST_Response
	 */
	public function generate_diagnostics_api_response() {
		Valet_Support_Diagnostics::get_instance()->get_all();
		return new WP_REST_Response( Valet_Support_Diagnostics::get_instance()->get_all() );
	}

	/**
	 * Valet User Exists response
	 *
	 * @return WP_REST_Response
	 */
	public function generate_valetusercheck_api_response() {
		$valet_user_exists = (bool) get_user_by( 'email', Valet_Support::get_instance()->get_user_email() );
		return new WP_REST_Response( $valet_user_exists );
	}
}
