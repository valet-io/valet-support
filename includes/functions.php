<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Misc Functions
 */

/**
 * No page caching on non-live Pantheon environments
 */
if ( isset( $_ENV['PANTHEON_ENVIRONMENT'] ) && 'live' !== $_ENV['PANTHEON_ENVIRONMENT'] ) {
	add_action( 'send_headers', 'valet_add_header_nocache', 15 );
}

function valet_add_header_nocache() {
	header( 'Cache-Control: no-cache, must-revalidate, max-age=0' );
}


/**
 * Have fatal error emails sent to Valet Support email box
 */
function valet_filter_recivery_mode_email( $email, $url ) {
	$email['to'] = Valet_Support::get_instance()->get_user_email();
	return $email;
}

add_filter( 'recovery_mode_email', 'valet_filter_recivery_mode_email', 10, 2 );

