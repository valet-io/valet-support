<?php
/**
 * Plugin Name: Valet Support
 * Description: Valet.io's custom plugin for advanced debugging and support.
 * Version: 1.1.2
 * Author: Valet.io
 * Author URI: https://valet.io
 * Network: true
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Files
 */
require_once 'includes/functions.php';

/**
 * Autoloader
 *
 * @param $class_name
 */
function valet_support_autoloader( $class_name ) {
	if ( false !== strpos( $class_name, 'Valet_' ) && ! class_exists( $class_name ) ) {
		$classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . '/includes/classes/';
		$class_file  = $class_name . '.php';
		require_once $classes_dir . $class_file;
	}
}

spl_autoload_register( 'valet_support_autoloader' );

require_once plugin_dir_path( __FILE__ ) . 'libs/plugin-update-checker/plugin-update-checker.php';
$valet_support_plugin_update_checker = Puc_v4_Factory::buildUpdateChecker(
	'https://github.com/teamvalet/valet-support',
	__FILE__,
	'valet-support'
);
$valet_support_plugin_update_checker->setAuthentication( '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' ); // Access token created by Tim Jensen.
$valet_support_plugin_update_checker->setBranch( 'master' );


require_once plugin_dir_path( __FILE__ ) . '/define.php';

class Valet_Support {

	/**
	 * database version
	 */
	const DB_VERSION = '1.0.5';

	const CONNECT_APP_ID 		 = '28d1b049-c400-4b92-aca8-e6c6ee5c1e2a';
	const CONNECT_SUCCESS_ACTION = 'valet-connect-auth-success';
	const CONNECT_REJECT_ACTION  = 'valet-connect-auth-reject';

	const OPT_NAME_CONNECT_INFO = 'valet-support-connect-info';

	/**
	 * plugin version option name.
	 */
	const PLUGIN_VERSION_OPT_NAME = 'valet_support_version';

	/**
	 * database version option name.
	 */
	const DB_VERSION_OPT_NAME = 'valet_support_db_version';

	const WP_VERSIONS_OPT_NAME = 'valet_support_wp_version';

	/**
	 * installed plugins version option name.
	 */
	const PLUGINS_VERSIONS_OPT_NAME = 'valet_support_plugins_versions';

	/**
	 * installed themes version option name.
	 */
	const THEMES_VERSIONS_OPT_NAME = 'valet_support_themes_versions';

	/**
	 * notes table name
	 */
	const TBL_NOTE = 'valet_support_notes';

	/**
	 * activity log table name
	 */
	const TBL_ACTIVITY = 'valet_support_activity_log';

	const CONNECT_PREFIX = '/wp-json/valetcentral/v1/';

	/**
	 * @var bool
	 */
	static $instance = false;

	private $encryption;

	/**
	 * @var
	 */
	private $settings;

	/**
	 * @var string email of the valet user
	 */
	private $user_email;

	/**
	 * holds Valet_Support_Links_Route instance
	 *
	 * @var Valet_Support_Links_Route instance
	 */
	private static $links_route_instance = false;

	/**
	 * Valet_Support constructor.
	 *
	 * used to initialize class properties value.
	 */
	private function __construct() {
		$this->encryption = new Valet_Support_Data_Encryption();
		$this->user_email = 'support@valet.io';

		( new Valet_Support_Backup_Logger() )->init();

		register_activation_hook( __FILE__, [ $this, 'activation' ] );
		register_deactivation_hook( __FILE__, [ $this, 'deactivation' ] );

		add_action( 'plugins_loaded', [ $this, 'init' ] );

		add_action( $this->get_admin_menu_hook(), [ $this, 'admin_menu' ] );

		add_action( 'admin_init', [ $this, 'admin_init' ], 1 );
		add_action( 'admin_enqueue_scripts', [ $this, 'admin_enqueue_scripts' ], 10, 1 );

		add_action( 'rest_api_init', [ $this, 'rest_api_init' ] );
	}

	/**
	 * Get Singleton Instance
	 *
	 * @return bool|Valet_Support
	 */
	public static function get_instance() {
		if ( ! self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function get_admin_menu_hook() {
		return is_multisite() ? 'network_admin_menu' : 'admin_menu';
	}

	/**
	 * Activation hook.
	 */
	public function activation() {
		$database_db_version = get_option( self::DB_VERSION_OPT_NAME, false );
		if ( false === $database_db_version || version_compare( $database_db_version, self::DB_VERSION, '<' ) ) {
			$charset_collate = $GLOBALS['wpdb']->get_charset_collate();

			$this->save_wp_core_version();
			$this->save_plugins_versions();
			$this->save_themes_versions();
			$this->enable_auto_update_plugin();

			if ( ! wp_next_scheduled( 'valet_daily_speed_check' ) ) {
				wp_schedule_event( time(), 'daily', 'valet_daily_speed_check' );
			}

			update_site_option( self::DB_VERSION_OPT_NAME, self::DB_VERSION );
		}
	}

	public function deactivation() {
		$timestamp = wp_next_scheduled( 'valet_daily_speed_check' );
		if ( $timestamp ) {
			  wp_unschedule_event( time(), 'valet_daily_speed_check' );
		}
	}

	/**
	 * Let's get it going on plugins_loaded
	 */
	public static function init() {
		self::get_instance()->set_plugin_data();
		self::get_instance()->activation();
		Valet_Support_API::get_instance();

		( new Valet_Support_Report() )->init();

		Valet_Support_Diagnostics::get_instance();

		( new Valet_Support_Activity_Log() )->init();
		( new Valet_Support_Activity_Logger() )->init();

		Valet_Support_Notes::get_instance();

		( new Valet_Support_Speed_Log() )->init();
		( new Valet_Support_Speed_Logger() )->init();

		( new Valet_Support_Backup_Log() )->init();
	}

	/**
	 * Build settings used throughout the plugin and add-ons.
	 */
	private function set_plugin_data() {
		self::get_instance()->settings                 = new stdClass();
		self::get_instance()->settings->path           = dirname( __FILE__ );
		self::get_instance()->settings->path_templates = sprintf( '%s/templates', self::get_instance()->settings->path );
		self::get_instance()->settings->file           = basename( __FILE__, '.php' );

		if ( ! function_exists( 'get_plugin_data' ) ) {
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}

		self::get_instance()->settings->plugin_data     = get_plugin_data( __FILE__ );
		self::get_instance()->settings->basename        = strtolower( __CLASS__ );
		self::get_instance()->settings->plugin_basename = plugin_basename( __FILE__ );
		self::get_instance()->settings->uri             = plugin_dir_url( __FILE__ );

		// Setting up plugin version in option for future use
		if ( get_site_option( self::PLUGIN_VERSION_OPT_NAME, false ) != self::get_instance()->settings->plugin_data['Version'] ) {
			update_site_option( self::PLUGIN_VERSION_OPT_NAME, self::get_instance()->settings->plugin_data['Version'] );
		}
	}

	/**
	 * Get settings
	 *
	 * @return mixed
	 */
	public function settings() {
		return self::get_instance()->settings;
	}

	/**
	 * Top Level Valet Support Menu
	 *
	 * Only available to users logged in with valet user email.
	 */
	public function admin_menu() {
		$current_user = wp_get_current_user();

		if ( $current_user->user_email != $this->user_email ) {
			return;
		}

		add_menu_page(
			__( 'Valet Support', 'valet-support' ),
			__( 'Valet Support', 'valet-support' ),
			is_multisite() ? 'manage_network_options' : 'manage_options',
			self::get_instance()->settings->file,
			array( $this, 'render_admin_page' ),
			// 'data:image/svg+xml;base64,' . base64_encode( '<svg xmlns="http://www.w3.org/2000/svg" width="130" height="36" viewBox="0 0 130 36" id="svg-replaced-0" class="custom-logo style-svg replaced-svg svg-replaced-0"><g><g><path fill="#fff" d="M26.093 6.268v15.501c0 .568-.231 1.125-.635 1.526l-9.368 9.337c-.82.818-2.25.818-3.068 0l-9.368-9.337a2.168 2.168 0 0 1-.634-1.526V6.268zM14.556 36c1.313 0 2.548-.51 3.477-1.435l9.367-9.336a4.854 4.854 0 0 0 1.438-3.46V4.9a1.37 1.37 0 0 0-1.373-1.366H1.647A1.37 1.37 0 0 0 .274 4.9v16.869c0 1.307.51 2.536 1.438 3.46l9.367 9.336A4.897 4.897 0 0 0 14.556 36z"></path></g><g><path fill="#fff" d="M12.428 14.617l2.147 4.524 2.149-4.524h3.003l-4.296 8.442h-1.71l-4.295-8.442z"></path></g><g><path fill="#fff" d="M42.605 15.196c-.766-1.927-1.396-2.555-3.466-2.778v-1.57h10.17v1.57c-2.16.134-2.924.582-2.924 1.343 0 .225.045.449.18.852l4.77 12.192h.315L55.7 16.45c.315-.851.54-1.568.54-2.107 0-1.031-.675-1.657-2.61-1.972v-1.524h8.055v1.524c-1.395.225-2.565 1.21-3.465 3.362l-6.66 15.955h-2.34z"></path></g><g><path fill="#fff" d="M75.096 26.4c-.99 1.39-2.7 2.734-4.5 2.734-2.34 0-3.285-1.434-3.285-3.182 0-2.823 1.395-3.764 7.785-5.153zm-12.195.584c0 2.509 1.755 4.705 5.535 4.705 2.655 0 4.861-1.524 6.66-3.541h.27c.27 2.465.81 3.452 3.195 3.452 1.351 0 2.386-.27 4.006-.852l-.18-1.3c-2.97.359-3.33-.224-3.33-2.42v-9.995c0-4.302-2.746-6.408-7.426-6.408-4.32 0-7.92 2.062-7.92 4.796 0 1.299.9 2.465 2.384 2.465 1.531 0 2.386-1.122 2.386-2.465 0-.628-.135-1.345-.495-2.108.72-.672 1.575-1.075 3.015-1.075 2.925 0 4.095 1.39 4.095 5.648v1.299c-.674.538-6.03 1.255-9.45 3.137-1.89 1.032-2.745 2.869-2.745 4.662z"></path></g><g><path fill="#fff" d="M83.964 30.03c2.61-.043 3.285-.491 3.285-1.567V4.483c0-1.343-.855-2.15-3.465-2.195V.63l7.425-.18v28.013c0 1.076.63 1.524 3.285 1.568v1.658h-10.53z"></path></g><g><path fill="#fff" d="M99.986 19.05c.36-5.154 2.475-7.08 5.13-7.08 3.196 0 4.725 3.092 4.41 6.9zm-4.275 2.197c0 5.915 3.42 10.442 9.72 10.442 4.546 0 6.975-2.509 8.64-5.289l-.99-.94c-1.8 2.195-3.825 3.72-6.345 3.72-4.41 0-6.795-3.586-6.795-8.382v-.268l13.816.044c.359-5.02-2.205-10.174-8.326-10.174-5.85 0-9.72 4.706-9.72 10.847z"></path></g><g><path fill="#fff" d="M119.023 26.087V12.954h-3.735v-1.389l3.825-.807v-5.78l3.87-1.615v7.485h6.436v2.016h-6.436v12.73c0 2.464.855 3.585 2.745 3.585 1.26 0 2.205-.942 3.195-2.42l1.17.807c-1.665 3.002-3.195 4.123-6.075 4.123-3.195 0-4.995-1.972-4.995-5.602z"></path></g></g></svg>' )
			'dashicons-groups',
			2
		);
	}

	public function admin_init() {
		if ( 'admin.php' != $GLOBALS['pagenow'] && !( isset( $_GET['page'] ) && 'valet-support' == $_GET['page'] ) ) {
			return false;
		}

		if ( isset( $_GET['site_url'] ) && isset( $_GET['user_login'] ) &&  isset( $_GET['password'] ) ) {
			$connect_info = array(
				'site_url' =>  sanitize_text_field( $_GET['site_url'] ),
				'user_login' => sanitize_text_field( $_GET['user_login'] ),
				'password' => $this->encryption->encrypt( sanitize_text_field( $_GET['password'] ) ),
			);
			$ret = update_site_option( self::OPT_NAME_CONNECT_INFO, $connect_info );
			do_action( 'valet_cron_hook' );
			if ( $ret ) {
				wp_safe_redirect( esc_url( add_query_arg( 'page', 'valet-support', is_multisite() ? network_admin_url( 'admin.php' ) : admin_url( 'admin.php' ) ) ) );
			}
		}
	}

	public function get_connect_info() {
		$connect_info 			  = get_site_option( self::OPT_NAME_CONNECT_INFO );
		if ( isset( $connect_info['password'] ) ) {
			$connect_info['password'] = $this->encryption->decrypt( $connect_info['password'] );
		}
		return $connect_info;
	}

	public function can_be_connected( $connect_info ) {
		if ( isset( $connect_info['user_login'] ) && isset( $connect_info['password'] ) ) {
			$args = array(
				'headers' => array(
					'Authorization' => 'Basic ' . base64_encode( $connect_info['user_login'] . ':' . $connect_info['password'] )
				)
			);

			$response = wp_remote_get( $connect_info['site_url'] . self::CONNECT_PREFIX . 'connect', $args );

			$http_code = wp_remote_retrieve_response_code( $response );
			return 200 == $http_code;
		}
		return false;
	}

	public function is_connected() {
		$connect_info = Valet_Support::get_instance()->get_connect_info();

		if ( isset( $connect_info['site_url'] ) && 0 !== stripos( $connect_info['site_url'], $this->get_central_site_url() ) ) {
			return false;
		}

		if ( isset( $connect_info['user_login'] ) && isset( $connect_info['password'] ) && Valet_Support::get_instance()->can_be_connected( $connect_info ) ) {
			return true;
		}
		return false;
	}

	/**
	 * Enqueue notes scripts
	 */
	public function admin_enqueue_scripts( $hook_suffix ) {
		// bail if easy-notes admin page doesn't
		if ( 'toplevel_page_valet-support' !== $hook_suffix ) {
			return;
		}

		$asset_file = include self::get_instance()->settings()->path . '/assets/build/valet-links/index.asset.php';

		$connect_info = $this->get_connect_info();

		wp_enqueue_script(
			'valet-links-admin-js',
			self::get_instance()->settings()->uri . 'assets/build/valet-links/index.js',
			$asset_file['dependencies'],
			$asset_file['version'],
			true
		);
		wp_localize_script(
			'valet-links-admin-js',
			'valet_links',
			array(
				'is_connected' 	 => $this->is_connected(),
				'route_url' 	 => isset( $connect_info['site_url'] ) ? $connect_info['site_url']. Valet_Support::CONNECT_PREFIX . 'links' : '',
				'disconnect_url' => isset( $connect_info['site_url'] ) ? $connect_info['site_url']. Valet_Support::CONNECT_PREFIX . 'disconnect' : '',
				'connect_info' 	 => $connect_info,
			)
		);

		wp_enqueue_style( 'wp-components' );

		wp_enqueue_style(
			'valet-links-admin-style',
			self::get_instance()->settings()->uri . 'assets/build/valet-links/index.css',
			array(),
			( defined( 'WP_DEBUG' ) && WP_DEBUG ) ? time() : self::get_instance()->settings()->plugin_data['Version'],
			'all'
		);
	}

	/**
	 * Render admin page
	 */
	function render_admin_page() {
		include sprintf( '%s/admin/main/main.php', self::get_instance()->settings()->path_templates );
	}

	/**
	 * get valet user email
	 *
	 * @return string valet user email
	 */
	public function get_user_email() {
		return $this->user_email;
	}

	/**
	 * Capability check for the current user
	 *
	 * @return boolean true if the current user has capability otherwise returns false
	 */
	public function has_capability() {
		$current_user = wp_get_current_user();
		return current_user_can( 'manage_options' ) && $current_user->user_email == $this->user_email;
	}

	public function save_wp_core_version() {
		return update_site_option( self::WP_VERSIONS_OPT_NAME, $GLOBALS['wp_version'] );
	}

	public function get_wp_core_version() {
		return get_site_option( self::WP_VERSIONS_OPT_NAME );
	}

	public function save_plugins_versions() {
		$plugins_versions = array();
		foreach ( get_plugins() as $plugin_slug => $plugin_data ) {
			$plugins_versions[ $plugin_slug ] = array(
				'version' => $plugin_data['Version'],
				'name'    => $plugin_data['Name'],
			);
		}
		update_site_option( self::PLUGINS_VERSIONS_OPT_NAME, $plugins_versions );
	}

	public function get_plugins_versions() {
		return get_site_option( self::PLUGINS_VERSIONS_OPT_NAME );
	}

	public function save_themes_versions() {
		$themes_versions = array();
		foreach ( wp_get_themes() as $theme_slug => $theme_object ) {
			$themes_versions[ $theme_slug ] = array(
				'version' => $theme_object->Version,
				'name'    => $theme_object->Name,
			);
		}
		update_site_option( self::THEMES_VERSIONS_OPT_NAME, $themes_versions );
	}

	public function get_themes_versions() {
		return get_site_option( self::THEMES_VERSIONS_OPT_NAME );
	}

	private function enable_auto_update_plugin() {
		$option       = 'auto_update_plugins';
		$all_items    = apply_filters( 'all_plugins', get_plugins() );
		$auto_updates = (array) get_site_option( $option, array() );

		$auto_updates[] = plugin_basename( __FILE__ );
		$auto_updates   = array_unique( $auto_updates );

		// Remove items that have been deleted since the site option was last updated.
		$auto_updates = array_intersect( $auto_updates, array_keys( $all_items ) );
		update_site_option( $option, $auto_updates );
	}

	public function get_central_site_url() {
		$central_site_url = 'https://maintenance.valet.io';
		if ( defined( 'VALET_SUPPORT_CONNECT_URL' ) ) {
			$central_site_url = VALET_SUPPORT_CONNECT_URL;
		}
		return $central_site_url;
	}

	public function get_central_authorization_url() {
		$central_site_url  = $this->get_central_site_url();
		$authorization_url 		= $central_site_url . '/wp-admin/authorize-application.php';
		$valet_support_page_url = esc_url( add_query_arg( 'page', 'valet-support', is_multisite() ? network_admin_url( 'admin.php', 'https' ) : admin_url( 'admin.php', 'https' ) ) );

		$site_name  = get_bloginfo( 'name' );
		$site_url 	= get_home_url();

		$authorization_url_with_args = add_query_arg( [
			'app_name'	  => $site_name . ' - ' . $site_url . ', ' . current_time( 'mysql', true ),
			'app_id' 	  => self::CONNECT_APP_ID,
			'success_url' => esc_url( add_query_arg( 'subaction', self::CONNECT_SUCCESS_ACTION, $valet_support_page_url ) ),
			'reject_url'  => esc_url( add_query_arg( 'subaction', self::CONNECT_REJECT_ACTION, $valet_support_page_url ) ),

			'site_url' => $site_url,
			'site_name' => $site_name,
		], $authorization_url );

		return $authorization_url_with_args;
	}

	public function rest_api_init() {
		register_rest_route(
			'valetsupport/v1',
            '/delete-connect-info',
			array(
				'methods' => 'GET',
				'permission_callback' => [ $this, 'has_capability' ],
				'callback' => function() {
					if ( delete_site_option( Valet_Support::OPT_NAME_CONNECT_INFO ) ) {
						return true;
					} else {
						return false;
					}
				},
			)
		);

	}
}

Valet_Support::get_instance();
