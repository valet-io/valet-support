import { render } from '@wordpress/element';
import ActivityLogs from './ActivityLogs';

import './styles/index.css';

if ( null !== document.getElementById( 'valet-support-activity-logs-wrapper' ) ) {
	render(
		<ActivityLogs />,
		document.getElementById( 'valet-support-activity-logs-wrapper' )
	);
}
