import { Component, Fragment } from '@wordpress/element';
import Interweave from 'Interweave';
import moment from 'moment';
import apiFetch from '@wordpress/api-fetch';

export default class SingleLog extends Component {
	constructor( props ) {
		super( props );
	}

	render() {
		const moment_local = moment.utc( this.props.create_date ).local();

		let iconClass = '';
		let objectTypeLabel = '';
		let objectName = '';

		let versionLabel = 'Version: ';
		switch ( this.props.object_type ) {
			case 'plugin':
				iconClass = 'dashicons-admin-plugins';
				objectTypeLabel = 'Plugin';
				objectName = this.props.object_name;
				break;

			case 'theme':
				iconClass = 'dashicons-admin-appearance';
				objectTypeLabel = 'Theme';
				objectName = this.props.object_name;
				break;

			case 'wp-core':
				iconClass = 'dashicons-wordpress';
				objectTypeLabel = 'WordPress Core';
				break;

			case 'user':
				iconClass = 'dashicons-admin-users';
				objectTypeLabel = 'User';
				objectName = this.props.object_name;
				versionLabel = 'Reassign content to User:';
				break;
		}

		let action = '';
		switch ( this.props.action ) {
			case 'auto-update':
			case 'update':
				action = 'Updated';
				break;

			case 'install':
				action = 'Installed';
				break;

			case 'network-activate':
				action = 'Network Activated';
				break;

			case 'activate':
				action = 'Activated';
				break;

			case 'deactivate':
				action = 'Deactivated';
				break;

			case 'network-deactivate':
				action = 'Network Deactivated';
				break;

			case 'uninstall':
				action = 'Uninstalled';
				break;

			case 'delete':
				action = 'Deleted';
				break;

			case 'create':
				action = 'Created';
				break;

			case 'switch_theme':
				action = 'Switched Theme';
				break;

			// Network user activity
			case 'wpmu-new-user':
				action = 'Created Network User';
				break;
			
			case 'wpmu-delete-user':
				action = 'Deleted Network User';
				break;

			case 'granted-super-admin':
				action = 'Granted Super Administrator Privileges of MultiSite Network';
				break;

			case 'revoked-super-admin':
				action = 'Revoked Super Administrator Privileges of MultiSite Network';
				break;

			case 'add-user-to-blog':
				action = 'Added User';
				break;

			case 'remove-user-from-blog':
				action = 'Deleted User';
				break;
			
			case 'wpmu-signup-user':
				action = 'Invited User';
				break;

			case 'wpmu-activate-user':
				action = 'Activate New User';
				break;
		}

		const site_specific_actions = [
			'wpmu-activate-user',
			'activate',
			'deactivate',
			'add-user-to-blog',
			'invite-user',
			'wpmu-signup-user',
			'remove-user-from-blog'
		];
		if (
			site_specific_actions.indexOf( this.props.action ) !== -1 &&
			'1' == this.props.is_multisite &&
			this.props.site_id > 0
		) {
			action +=
				' in subsite "' +
				this.props.site_name +
				'" (site ID: ' +
				this.props.site_id +
				')';
		}

		let version = '';
		switch ( this.props.action ) {
			case 'auto-update':
			case 'update':
			case 'switch_theme':
				version =
					'From ' +
					this.props.object_subtype_from +
					' to ' +
					this.props.object_subtype_to;
				break;

			default:
				if ( this.props.object_subtype_to ) {
					version = this.props.object_subtype_to;
				}
		}

		return (
			<div className="single-activity-log">
				<div className="single-log-first-container">
					<div>
						<span className={ 'dashicons ' + iconClass }> </span>
						<span className="object-type">
							{ ' ' }
							<strong> { objectTypeLabel } </strong>{ ' ' }
						</span>
						{ objectName && (
							<span>
								{ ' ' }
								<strong>
									{ ' ' }
									<Interweave content={ objectName } />{ ' ' }
								</strong>{ ' ' }
							</span>
						) }
					</div>
					{ this.props.user_id_of_action_by > 0 ? (
						<div>
							Action By:{ ' ' }
							<strong> { this.props.display_name_of_action_by + ' [Email: ' + this.props.email_address_of_action_by + ', ID: ' + this.props.user_id_of_action_by + ']' } </strong>
						</div>
					) : (
						<div></div>
					) }
					<div>
						Time:{ ' ' }
						<strong>
							{ ' ' }
							{ moment_local.format(
								'MMMM Do YYYY, dddd, h:mm:ss a'
							) }{ ' ' }
						</strong>{ ' ' }
						({ moment_local.fromNow() })
					</div>
				</div>

				<div className="single-log-second-container">
					<div>
						Action: <strong> { action } </strong>
					</div>
					{ version ? (
						<div>
							{ versionLabel }{ ' ' }
							<strong>
								{ ' ' }
								<Interweave content={ version } />{ ' ' }
							</strong>
						</div>
					) : (
						<div></div>
					) }
					<div>
						IP: <strong> { this.props.ip } </strong>
					</div>
				</div>
			</div>
		);
	}
}
