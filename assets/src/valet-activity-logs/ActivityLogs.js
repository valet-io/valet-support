import { Component, Fragment } from '@wordpress/element';
import { Button, Spinner } from '@wordpress/components';

import { encode } from "base-64";
import SingleLog from './SingleLog';

export default class ActivityLogs extends Component {
	constructor( props ) {
		super( props );
		this.state = {
			logs: [],
			searchObjectType: '',
			search: '',
			isLoading: true,
		};
	}

	componentDidMount() {
		this.loadLogs();
	}

	loadLogs = () => {
		fetch( `${valet_activity_logs.route_url}?search_object_type=${this.state.searchObjectType}&search=${this.state.search}`, {
            method: 'GET',
            cache: 'no-cache',
            headers: new Headers({
                'Authorization': 'Basic ' + encode( valet_activity_logs.connect_info.user_login + ":" + valet_activity_logs.connect_info.password ),
                'Content-Type': 'application/json'
            }),
        })
        .then( response => {
			this.setState( { 
							total: response.headers.get( 'X-WP-Total' ),
						} );
			return response.json() 
		})
        .then( res => {
            this.setState( {
				logs: res,
				isLoading: false,
			} );
        })
        .catch((error) => {
            console.error( 'Error:' );
            console.error( error );
            this.setState( {
				isLoading: false,
			} );
			alert(
				'Error in fetching activity logs list with the message: ' +
					error.message +
					'(' +
					error.code +
					')'
			);
        });
	};

	handleSearchObjectTypeChange = ( event ) => {
		this.setState( { 
			searchObjectType: event.target.value,
		} );
	}

	handleSearchChange = ( event ) => {
		this.setState(
			{ 
				search: event.target.value
			}
		);
	}

	handleSearchKeyUp = ( event ) => {
		if ( 13 == event.keyCode ) {
			event.preventDefault();
			this.loadLogs();
		}
	}

	handleSearchClick = () => {
		this.loadLogs();
	}

	handleSearchClearClick = ( event ) => {
		event.preventDefault();
		this.setState(
			{
				search: '',
				searchObjectType: '',
			},
			() => this.loadLogs()
		);
		
	}

	render() {
		return (
			<Fragment>
				<h1>Activity Logs</h1>
				{ false === this.state.isLoading && this.state.total > 0 &&
					(
						<div class="valet-support search-wrapper">
							<label for="search-object-type">
								Entity:
							</label>
							<select id="search-object-type" name="search-object-type" onChange={this.handleSearchObjectTypeChange}>
								<option value="">All</option>
								<option value="user">User</option>
								<option value="plugin">Plugin</option>
								<option value="theme">Theme</option>
							</select>
							&nbsp;&nbsp;
							<label for="search">Search: </label>
							<input type="text" id="search" className="valet-support search-input-text" value={ this.state.search } placeholder="Search" onChange={ this.handleSearchChange } onKeyUp={ this.handleSearchKeyUp } size="64" />

							<Button isSecondary className="is-button valet-support search-button" isSmall={ true } onClick={ this.handleSearchClick }>Search</Button>
							<Button className="is-button valet-support search-button" isSmall={ true } isDestructive onClick={ this.handleSearchClearClick }>Clear</Button>
						</div>
					)
				}
				<div className="activity-log-container">
					{ this.state.isLoading && (
						<div className="valet-notes-loader">
							<Spinner color="blue" size="200px" />
						</div>
					) }
					{ !this.state.isLoading && 0 === this.state.logs.length && (
						<strong>
							{ '' == this.state.searchObjectType && '' == this.state.search &&
							(
								<Fragment>No activity log exists now.</Fragment>
							)}
							{ ( '' != this.state.searchObjectType || '' != this.state.search ) &&
							(
								<Fragment>No activity log found!</Fragment>
							)}
						</strong>
					) }
					{ this.state.logs.map( ( log ) => (
						<SingleLog { ...log } />
					) ) }
				</div>
			</Fragment>
		);
	}
}
