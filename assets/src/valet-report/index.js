import { render } from '@wordpress/element';
import Report from './Report';

import './styles/index.css';

if ( null !== document.getElementById( 'valet-support-report-wrapper' ) ) {
	render(
		<Report />,
		document.getElementById( 'valet-support-report-wrapper' )
	);
}
