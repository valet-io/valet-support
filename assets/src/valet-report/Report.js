import { Component, Fragment } from '@wordpress/element';
import { Button, Spinner } from '@wordpress/components';

import moment from 'moment';
import { encode } from "base-64";

export default class Report extends Component {
	constructor( props ) {
		super( props );
		this.state = {
			reportURL: '',
			isLoading: '',
			time_period: 'this_week',
			from_date: '',
			to_date: '',
		};
	}

	downloadReport = (event) => {
		event.preventDefault();

		if ('custom' === this.state.time_period) {
			if ('' === this.state.from_date) {
				alert('The from date should not be empty.');
				return;
			}
			if ('' === this.state.to_date) {
				alert('The to date should not be empty.');
				return;
			}

			if (moment(this.state.from_date, 'DD/MM/YYYY', true).isValid()) {
				alert('The from date is not in a valid format.');
				return;
			}

			if (moment(this.state.to_date, 'DD/MM/YYYY', true).isValid()) {
				alert('The to date is not in a valid format.');
				return;
			}
			
			const timestamp_current = new Date().getTime() + ( 23 * 60 * 60 * 1000 )
			const timestamp_90_days_older = new Date().getTime() - (90 * 24 * 60 * 60 * 1000);
			const from_timestamp = new Date(this.state.from_date).getTime();
			//from date and to date valid format check

			if (from_timestamp < timestamp_90_days_older) {
				alert("You can't generate a report of a date range older than 90 days.")
				return;
			}

			const to_timestamp = new Date(this.state.to_date).getTime();
			if (from_timestamp > to_timestamp) {
				alert("The from date should not be greater than the to date.")
				return;
			}

			if (from_timestamp > timestamp_current) {
				alert("The from date should not be in the future.")
				return;
			}

			if (to_timestamp > timestamp_current) {
				alert("The to date should not be in the future.")
				return;
			}
		}

		this.setState( {
			isLoading: true
		} );
		fetch( valet_report.route_url , {
            method: 'POST',
            headers: new Headers({
                'Authorization': 'Basic ' + encode( valet_report.connect_info.user_login + ":" + valet_report.connect_info.password ),
                'Content-Type': 'application/json'
            }),
			body: JSON.stringify( {
				time_period: this.state.time_period,
				from_date: this.state.from_date,
				to_date: this.state.to_date,
				gmt_offset: valet_report.gmt_offset,
				timezone_string: valet_report.timezone_string,
				installed_plugins_count: valet_report.installed_plugins_count,
				installed_themes_count: valet_report.installed_themes_count,
				wp_version: valet_report.wp_version,
				posts_count: valet_report.posts_count,
				comments_count: valet_report.comments_count,
				users_count: valet_report.users_count,
				administrator_role_user_count: valet_report.administrator_role_user_count
			} )
        })
        .then( response => {
			return response.json() 
		})
        .then( res => {
			console.log( res );
            this.setState( {
				isLoading: false,
				reportURL: res.report_url
			} );
			// alert( res.report_url );

			let download_anchor = document.getElementById('valet-report-download')
			download_anchor.href = res.report_url
			download_anchor.click()
			
			console.log( res.report_url )
        })
        .catch((error) => {
            console.error( 'Error:' );
            console.error( error );
            this.setState( {
				isLoading: false,
			} );
			alert(
				'Error in generating report with the message: ' +
					error.message +
					'(' +
					error.code +
					')'
			);
        });
	};

	handleChange = e => {
		const { name, value } = e.target;
		// console.log( name + ' -> ' + value );	
		this.setState({
		  [name]: value
		});
	};

	render() {
		return (
			<Fragment>
				<h1>Generate Report</h1>
				<br /><br />
				<div className="report-container">
					<form id="report-form">
						<strong>Time Period: </strong>
						<label for="time-period-this-week">
							<input type="radio" id="time-period-this-week" name="time_period" value="this_week" checked={this.state.time_period === 'this_week'} onChange={this.handleChange} /> This week
						</label>

						<label for="time-period-last-week">
							<input type="radio" id="time-period-last-week" name="time_period" value="last_week" checked={this.state.time_period === 'last_week'} onChange={this.handleChange} /> Last week
						</label>

						<label for="time-period-this-month">
							<input type="radio" id="time-period-this-month" name="time_period" value="this_month" checked={this.state.time_period === 'this_month'} onChange={this.handleChange} /> This Month
						</label>

						<label for="time-period-last-month">
							<input type="radio" id="time-period-last-month" name="time_period" value="last_month" checked={this.state.time_period === 'last_month'} onChange={this.handleChange} /> Last Month
						</label>

						<label for="time-period-custom">
							<input type="radio" id="time-period-custom" name="time_period" value="custom" checked={this.state.time_period === 'custom'}  onChange={this.handleChange} /> Custom
						</label>

						{this.state.time_period === 'custom' && (
							<div className="custom-date-range-container">
								<strong>From: </strong>
								<input type="date" name="from_date" value={this.state.from_date} onChange={this.handleChange} />
								&nbsp;&nbsp;
								<strong>To: </strong>
								<input type="date" name="to_date" value={this.state.to_date} onChange={this.handleChange} />
								<div>You can't generate a report older than 90 days.</div>
							</div>
						)}
						
						{this.state.time_period !== 'custom' && (
							<br />	
						)}
						<button onClick={ this.downloadReport } className="button button-primary">
							{true === this.state.isLoading && 
								<Fragment>Generating Report</Fragment>
							}
							{true !== this.state.isLoading && 
								<Fragment>Download Report</Fragment>
							}
						</button>
					</form>
					{ this.state.isLoading && (
						<div className="valet-report-loader">
							<Spinner color="blue" size="200px" />
						</div>
					) }
					
				</div>
			</Fragment>
		);
	}
}
