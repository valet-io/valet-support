import { Component, Fragment } from '@wordpress/element';
import { encode } from "base-64";

import SingleLinkButtons from './SingleLinkButtons';
import EditLink from './EditLink';

export default class SingleLink extends Component {
	constructor( props ) {
		super( props );
		this.state = {
			url: '',
			title: '',
			editURL: '',
			editTitle: '',
			isEditing: false,
			errorMessage: '',
		};
	}

	static getDerivedStateFromProps( props, state ) {
		return {
			url: props.url,
			title: props.title,
		};
	}

	handleEditLink = ( link_id ) => {
		this.setState( {
			isEditing: true,
			editURL: this.state.url,
			editTitle: this.state.title,
		} );
	};

	handleEditURLChange = ( event ) => {
		const URLVal = event.target.value;
		const newState = { editURL: URLVal };
		if ( '' == this.state.editURL ) {
			newState.errorMessage = '';
		}
		this.setState( newState );
	};

	handleEditTitleChange = ( event ) => {
		const titleVal = event.target.value;
		const newState = { editTitle: titleVal };
		if ( '' == this.state.title ) {
			newState.errorMessage = '';
		}
		this.setState( newState );
	};

	handleUpdateLink = () => {
		const trimmedURL = this.state.editURL.trim();
		const trimmedTitle = this.state.editTitle.trim();
		const pattern = new RegExp(
			'(https?:\\/\\/)?((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|((\\d{1,3}\\.){3}\\d{1,3}))(\\:\\d+)?(\\/[-a-z\\d%_.~+@]*)*(\\?[;&a-z\\d%_.~+=-@]*)?(\\#[-a-z\\d_@]*)?$',
			'i'
		);

		if ( '' == trimmedURL || '' == trimmedTitle ) {
			if ( '' == trimmedURL && '' == trimmedTitle ) {
				this.setState( {
					errorMessage:
						'Link URL and Link Title should not be empty!',
				} );
			} else if ( '' == trimmedURL ) {
				this.setState( {
					errorMessage: 'Link URL should not be empty!',
				} );
			} else if ( '' == trimmedTitle ) {
				this.setState( {
					errorMessage: 'Link Title should not be empty!',
				} );
			}
		} else if ( ! pattern.test( trimmedURL ) ) {
			this.setState( { errorMessage: 'Link URL should be valid URL!' } );
		} else {
			const data = {
				id: this.props.id,
				url: trimmedURL,
				title: trimmedTitle,
			};

			fetch( valet_links.route_url + '/' + this.props.id, {
				method: 'PUT',
				headers: new Headers({
					'Authorization': 'Basic ' + encode( valet_links.connect_info.user_login + ":" + valet_links.connect_info.password ),
					'Content-Type': 'application/json'
				}),
				body: JSON.stringify( data ),
			})
			.then( response => response.json() )
			.then( res => {
				if ( true === res ) {
					this.props.updateContentOfLink( data );
					this.setState( {
						editURL: '',
						editTitle: '',
						isEditing: false,
					} );
				}
			})
			.catch((error) => {
				console.error('Error:', error);
				alert(
					'Error in updating the link with the message: ' +
						error.message +
						'(' +
						error.code +
						')'
				);
			});
		}
	};

	handleCancelEditLink = () => {
		this.setState( {
			isEditing: false,
		} );
	};

	render() {
		const { url, title, isEditing, errorMessage } = this.state;

		let ret;
		if ( isEditing ) {
			ret = (
				<EditLink
					// methods
					handleUpdateLink={ this.handleUpdateLink }
					handleCancelEditLink={ this.handleCancelEditLink }
					// handleChangeLinkTextArea={ this.handleChangeLinkTextArea }
					handleEditURLChange={ this.handleEditURLChange }
					handleEditTitleChange={ this.handleEditTitleChange }
					// vars
					id={ this.props.id }
					editURL={ this.state.editURL }
					editTitle={ this.state.editTitle }
					errorMessage={ errorMessage }
				/>
			);
		} else {
			ret = (
				<Fragment>
					<div>
						<a href={ url } target="_blank" rel="noreferrer">
							{ ' ' }
							{ title }{ ' ' }
						</a>
					</div>
					<SingleLinkButtons
						id={ this.props.id }
						handleDeleteLink={ this.props.handleDeleteLink }
						handleEditLink={ this.handleEditLink }
					/>
				</Fragment>
			);
		}

		return (
			<div
				className="card valet-link-single-container"
				key={ this.props.id }
			>
				{ ret }
			</div>
		);
	}
}
