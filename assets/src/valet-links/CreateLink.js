import { Component, Fragment } from '@wordpress/element';
import { Notice } from '@wordpress/components';
import { ButtonGroup, Button } from '@wordpress/components';

export default class CreateLink extends Component {
	render() {
		return (
			<div className="valet-link-container">
				{ this.props.successMessage && (
					<Notice status="success" isDismissible={ true }>
						<p>{ this.props.successMessage }</p>
					</Notice>
				) }

				<div className="card valet-link-single-container">
					<div className="valet-edit-link-container">
						<div>
							<form
								method="post"
								name="valet-support-link"
								id="valet-support-link"
								onSubmit={
									this.props.handleCreateLinkFormSubmit
								}
							>
								<div className="valet-edit-link-input-container">
									<div className="valet-edit-link-input">
										<label htmlFor="valet-url">URL:</label>
										<input
											type="url"
											onChange={
												this.props.handleCreateURLChange
											}
											value={ this.props.url }
											className="valet-url"
											id="valet-url"
										/>
									</div>
									<div className="valet-edit-link-input">
										<label htmlFor="valet-title">
											Title:
										</label>
										<input
											type="text"
											name="title"
											id="valet-title"
											onChange={
												this.props
													.handleCreateTitleChange
											}
											value={ this.props.title }
											className="valet-title"
										/>
									</div>
								</div>
								<ButtonGroup>
									<button
										type="submit"
										className="components-button is-button is-primary"
									>
										Add Link
									</button>
								</ButtonGroup>
							</form>
						</div>
						{ this.props.errorMessage && (
							<p className="valet-error">
								{ this.props.errorMessage }{ ' ' }
							</p>
						) }
					</div>
				</div>
			</div>
		);
	}
}
