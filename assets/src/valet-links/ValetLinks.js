import { Component, Fragment } from '@wordpress/element';
import { encode } from "base-64";

import CreateLink from './CreateLink';
import LinksList from './LinksList';

import './styles/LinkList.css';
import './styles/CreateLink.css';

export default class ValetLinks extends Component {
	constructor( props ) {
		super( props );
		this.state = {
			url: '',
			title: '',
			errorMessage: '',
			successMessage: '',
			links: [],
			isLoading: true,
		};
	}

	handleCreateLinkFormSubmit = ( event ) => {
		event.preventDefault();
		const trimmedURL = this.state.url.trim();
		const trimmedTitle = this.state.title.trim();
		const pattern = new RegExp(
			'(https?:\\/\\/)?((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|((\\d{1,3}\\.){3}\\d{1,3}))(\\:\\d+)?(\\/[-a-z\\d%_.~+@]*)*(\\?[;&a-z\\d%_.~+=-@]*)?(\\#[-a-z\\d_@]*)?$',
			'i'
		);

		if ( '' == trimmedURL || '' == trimmedTitle ) {
			if ( '' == trimmedURL && '' == trimmedTitle ) {
				this.setState( {
					errorMessage:
						'Link URL and Link Title should not be empty!',
				} );
			} else if ( '' == trimmedURL ) {
				this.setState( {
					errorMessage: 'Link URL should not be empty!',
				} );
			} else if ( '' == trimmedTitle ) {
				this.setState( {
					errorMessage: 'Link Title should not be empty!',
				} );
			}
		} else if ( ! pattern.test( trimmedURL ) ) {
			this.setState( { errorMessage: 'Link URL should be valid URL!' } );
		} else {
			fetch( valet_links.route_url, {
				method: 'POST',
				headers: new Headers({
					'Authorization': 'Basic ' + encode( valet_links.connect_info.user_login + ":" + valet_links.connect_info.password ),
					'Content-Type': 'application/json'
				}),
				body: JSON.stringify( { url: this.state.url, title: this.state.title } ),
			})
			.then( response => response.json() )
			.then( res => {
				const { links } = this.state;
				links.push( res );

				this.setState( {
					url: '',
					title: '',
					links,
					successMessage: 'Link added!',
				} );
				setTimeout( () => {
					this.setState( { successMessage: '' } );
				}, 6000 );
			})
			.catch((error) => {
				console.error('Error:', error);
				alert(
					'Error in creating the link with the message: ' +
						error.message +
						'(' +
						error.code +
						')'
				);
			});
		}
	};

	handleCreateURLChange = ( event ) => {
		const URLVal = event.target.value;
		const newState = { url: URLVal };
		if ( '' == this.state.url ) {
			newState.errorMessage = '';
		}
		this.setState( newState );
	};

	handleCreateTitleChange = ( event ) => {
		const titleVal = event.target.value;
		const newState = { title: titleVal };
		if ( '' == this.state.title ) {
			newState.errorMessage = '';
		}
		this.setState( newState );
	};

	componentDidMount() {
		this.loadLinks();
	}

	updateContentOfLink = ( data ) => {
		const { links } = this.state;
		const newLinks = links.map( ( link, index ) => {
			if ( link.id == data.id ) {
				link.url = data.url;
				link.title = data.title;
			}
			return link;
		} );
		this.setState( {
			links: newLinks,
		} );
	};

	loadLinks = () => {
		fetch( valet_links.route_url, {
			method: 'GET', // or 'PUT'
    		cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
			headers: new Headers({
				'Authorization': 'Basic ' + encode( valet_links.connect_info.user_login + ":" + valet_links.connect_info.password ),
				'Content-Type': 'application/json'
			}),
		})
		.then( response => response.json() )
		.then( res => {
			this.setState( {
				links: res,
				isLoading: false,
			} );
		})
		.catch((error) => {
			console.error( 'Error:' );
			console.error( error );
			alert(
				'Error in fetching links list with the message: ' +
					error.message +
					'(' +
					error.code +
					')'
			);
		});
	};

	handleDeleteLink = ( link_id ) => {
		if ( confirm( 'Are you sure you want to delete this link?' ) ) {
			fetch( valet_links.route_url + '/' + link_id, {
				method: 'DELETE',
				headers: new Headers({
					'Authorization': 'Basic ' + encode( valet_links.connect_info.user_login + ":" + valet_links.connect_info.password ),
					'Content-Type': 'application/json'
				})
			})
			.then( response => response.json() )
			.then( res => {
				if ( true === res ) {
					this.loadLinks();
				}
			})
			.catch((error) => {
				console.error('Error:', error);
				alert(
					'Error in deleting the link with the message: ' +
						error.message +
						'(' +
						error.code +
						')'
				);
			});

			/*
			apiFetch( {
				url: valet_links.route_url + '/' + link_id + '?_method=DELETE',
				method: 'GET',
				headers: {
					'X-HTTP-Method-Override': 'DELETE',
					'Content-Type': 'application/json'
				}
			} ).then(
				( res ) => {
					if ( true === res.data ) {
						this.loadLinks();
					}
				},
				( error ) => {
					alert(
						'Error in deleting the link with the message: ' +
							error.message +
							'(' +
							error.code +
							')'
					);
				}
			);
			*/
		}
	};

	render() {
		return (
			<Fragment>
				<CreateLink
					// methods
					handleCreateLinkFormSubmit={
						this.handleCreateLinkFormSubmit
					}
					handleCreateURLChange={ this.handleCreateURLChange }
					handleCreateTitleChange={ this.handleCreateTitleChange }
					// vars
					url={ this.state.url }
					title={ this.state.title }
					errorMessage={ this.state.errorMessage }
					successMessage={ this.state.successMessage }
				/>
				<hr />
				<LinksList
					// methods
					handleDeleteLink={ this.handleDeleteLink }
					updateContentOfLink={ this.updateContentOfLink }
					//vars
					links={ this.state.links }
					isLoading={ this.state.isLoading }
				/>
			</Fragment>
		);
	}
}
