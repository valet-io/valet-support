import { Component, Fragment } from '@wordpress/element';
import { ButtonGroup, Button } from '@wordpress/components';

export default class EditLink extends Component {
	constructor( props ) {
		super( props );
		this.state = {
			errorMessage: '',
		};
	}

	render() {
		const { handleUpdateLink, handleCancelEditLink } = this.props;
		return (
			<div className="valet-edit-link-container">
				<div>
					<div className="valet-edit-link-input-container">
						<div className="valet-edit-link-input">
							<label htmlFor="valet-edit-url">URL:</label>
							<input
								type="url"
								name="url"
								id="valet-edit-url"
								value={ this.props.editURL }
								onChange={ this.props.handleEditURLChange }
								className="valet-edit-url"
							/>
						</div>
						<div className="valet-edit-link-input">
							<label htmlFor="valet-edit-title">Title:</label>
							<input
								type="text"
								name="title"
								id="valet-edit-title"
								value={ this.props.editTitle }
								onChange={ this.props.handleEditTitleChange }
								className="valet-edit-title"
							/>
						</div>
					</div>
					<ButtonGroup>
						<Button
							onClick={ handleUpdateLink }
							isPrimary
							className="is-button"
						>
							Update
						</Button>
						<Button
							onClick={ handleCancelEditLink }
							isDestructive
							className="is-button"
						>
							Cancel
						</Button>
					</ButtonGroup>
				</div>
				{ this.props.errorMessage && (
					<p className="valet-error">{ this.props.errorMessage } </p>
				) }
			</div>
		);
	}
}
