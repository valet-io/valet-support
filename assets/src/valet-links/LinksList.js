import { Component } from '@wordpress/element';
import SingleLink from './SingleLink';
import { Spinner } from '@wordpress/components';

export default class LinksLists extends Component {
	render() {
		const { links, isLoading } = this.props;
		return (
			<div className="valet-link-container">
				{ isLoading && (
					<div className="valet-links-loader">
						{ ' ' }
						<Spinner color="blue" size="50" />{ ' ' }
					</div>
				) }
				{ links.map( ( link ) => (
					<SingleLink
						{ ...link }
						handleDeleteLink={ this.props.handleDeleteLink }
						updateContentOfLink={ this.props.updateContentOfLink }
					/>
				) ) }
			</div>
		);
	}
}
