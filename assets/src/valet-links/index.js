import { render } from '@wordpress/element';
import ValetLinks from './ValetLinks';
import apiFetch from '@wordpress/api-fetch';

import 'whatwg-fetch'
import { encode } from "base-64";

if ( null !== document.getElementById( 'valet-support-links-wrapper' ) ) {
	render(
		<ValetLinks />,
		document.getElementById( 'valet-support-links-wrapper' )
	);
}

window.valet_support_disconnect = function() { 
	if ( confirm( 'Are you sure to disconnect from the maintenance site and delete all data?' ) ) {
		fetch( valet_links.disconnect_url, {
			method: 'GET',
			headers: new Headers({
				'Authorization': 'Basic ' + encode( valet_links.connect_info.user_login + ":" + valet_links.connect_info.password ),
				'Content-Type': 'application/json'
			})
		})
		.then( response => response.json() )
		.then( res => {
			if ( true == res ) {
				apiFetch( {
					path: '/valetsupport/v1/delete-connect-info',
					method: 'GET'
				} ).then( ( res ) => {
					if ( true == res ) {
						location.reload();
					}
				} );
			}
		})
		.catch((error) => {
			console.error('Error:', error);
			alert(
				'Error in disconnecting the maintenance site with the message: ' +
					error.message +
					'(' +
					error.code +
					')'
			);
		});
	}
}
