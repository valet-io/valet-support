import { Component } from '@wordpress/element';
import { ButtonGroup, Button } from '@wordpress/components';

export default class SingleLinkButtons extends Component {
	render() {
		const { id } = this.props;
		return (
			<ButtonGroup>
				<Button
					onClick={ () => this.props.handleEditLink() }
					isSmall={ true }
				>
					<span className="dashicons dashicons-edit"> </span>
				</Button>
				<Button
					onClick={ () => this.props.handleDeleteLink( id ) }
					isPrimary
					isDestructive
					isSmall={ true }
				>
					<span className="dashicons dashicons-no-alt"> </span>
				</Button>
			</ButtonGroup>
		);
	}
}
