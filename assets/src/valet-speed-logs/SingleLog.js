import { Component, Fragment } from '@wordpress/element';
import Interweave from 'Interweave';
import moment from 'moment';
import apiFetch from '@wordpress/api-fetch';

export default class SingleLog extends Component {
	constructor( props ) {
		super( props );
	}

	render() {
		const moment_local = moment.utc( this.props.create_date ).local();

		let speed_index    = JSON.parse( this.props.speed_index );

		console.log( this.props.speed_index );
		
		return (
			<div className="single-speed-log">
				<div className="single-log-first-container">
					<div>
						Load Time:{ ' ' }
						<strong>
							{speed_index.displayValue}
						</strong>
						{
							'1' == this.props.is_multisite &&
							this.props.site_id > 0 &&
							(
								<div>
									Sub-Site Name: <strong>{this.props.site_name}</strong>
									{ ' ' }
									(Site ID: {this.props.site_id})
								</div>
							)
						}
					</div>
					
				</div>

				<div className="single-log-second-container">
					<div>
						Time:{ ' ' }
						<strong>
							{ ' ' }
							{ moment_local.format(
								'MMMM Do YYYY, dddd, h:mm:ss a'
							) }{ ' ' }
						</strong>{ ' ' }
						({ moment_local.fromNow() })
					</div>
				</div>
			</div>
		);
	}
}
