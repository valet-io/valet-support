import { Component, Fragment } from '@wordpress/element';
import { Button, Spinner } from '@wordpress/components';

import { encode } from "base-64";
import SingleLog from './SingleLog';

export default class SpeedLogs extends Component {
	constructor( props ) {
		super( props );
		this.state = {
			total: 0,
			logs: [],
			isLoading: true,
		};
	}

	componentDidMount() {
		this.loadLogs();
	}

	loadLogs = () => {
		fetch( `${valet_speed_logs.route_url}`, {
            method: 'GET',
            cache: 'no-cache',
            headers: new Headers({
                'Authorization': 'Basic ' + encode( valet_speed_logs.connect_info.user_login + ":" + valet_speed_logs.connect_info.password ),
                'Content-Type': 'application/json'
            }),
        })
        .then( response => {
			this.setState( { 
							total: response.headers.get( 'X-WP-Total' ),
						} );
			return response.json() 
		})
        .then( res => {
            this.setState( {
				logs: res,
				isLoading: false,
			} );
        })
        .catch((error) => {
            console.error( 'Error:' );
            console.error( error );
            this.setState( {
				isLoading: false,
			} );
			alert(
				'Error in fetching speed logs list with the message: ' +
					error.message +
					'(' +
					error.code +
					')'
			);
        });
	};

	render() {
		return (
			<Fragment>
				<h1>Speed Logs</h1>
				<div className="speed-log-container">
					{ this.state.isLoading && (
						<div className="valet-speed-logs-loader">
							<Spinner color="blue" size="200px" />
						</div>
					) }
					{ !this.state.isLoading && 0 === this.state.logs.length && (
						<strong>
							<Fragment>No speed log exists now.</Fragment>
						</strong>
					) }
					{ this.state.logs.map( ( log ) => (
						<SingleLog { ...log } />
					) ) }
				</div>
			</Fragment>
		);
	}
}
