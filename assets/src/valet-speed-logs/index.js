import { render } from '@wordpress/element';
import SpeedLogs from './SpeedLogs';

import './styles/index.css';

if ( null !== document.getElementById( 'valet-support-speed-logs-wrapper' ) ) {
	render(
		<SpeedLogs />,
		document.getElementById( 'valet-support-speed-logs-wrapper' )
	);
}
