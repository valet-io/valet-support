import { Component } from '@wordpress/element';
import { ButtonGroup, Button } from '@wordpress/components';

import SingleNoteButtons from './SingleNoteButtons';

export default class SingleNote extends Component {
	render() {
		const { id } = this.props;
		return (
			<ButtonGroup>
				<Button
					onClick={ () => this.props.handleEditNote() }
					isSmall={ true }
				>
					Edit
				</Button>
				<Button
					onClick={ () => this.props.handleDeleteNote( id ) }
					isPrimary
					isDestructive
					isSmall={ true }
				>
					Delete
				</Button>
			</ButtonGroup>
		);
	}
}
