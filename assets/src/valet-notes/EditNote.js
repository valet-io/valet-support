import { Component, Fragment } from '@wordpress/element';
import { ButtonGroup, Button } from '@wordpress/components';

export default class EditNote extends Component {
	constructor( props ) {
		super( props );
		this.state = {
			errorMessage: '',
		};
	}

	render() {
		const { handleUpdateNote, handleCancelEditNote } = this.props;
		return (
			<div>
				<div>
					<textarea
						onChange={ this.props.handleChangeNoteTextArea }
						value={ this.props.editNote }
						id="valet-note"
						rows="3"
						cols="55"
					/>
					{ this.props.errorMessage && (
						<p className="valet-error">
							{ ' ' }
							{ this.props.errorMessage }{ ' ' }
						</p>
					) }
				</div>
				<ButtonGroup>
					<Button
						onClick={ handleUpdateNote }
						isPrimary
						isSmall={ true }
					>
						Update
					</Button>
					<Button
						onClick={ handleCancelEditNote }
						isDestructive
						isSmall={ true }
					>
						Cancel
					</Button>
				</ButtonGroup>
			</div>
		);
	}
}
