import { Component, Fragment } from '@wordpress/element';
import { Button } from '@wordpress/components';
import { encode } from "base-64";

import CreateNote from './CreateNote';
import NotesList from './NotesList';

export default class ValetNotes extends Component {
	constructor( props ) {
		super( props );
		this.state = {
			note: '',
			errorMessage: '',
			successMessage: '',
			notes: [],
			search: '',
			total: 0,
			isLoading: true,
		};
	}

	handleCreateNoteFormSubmit = ( event ) => {
		event.preventDefault();
		const trimmedNote = this.state.note.trim();
		if ( '' == trimmedNote ) {
			this.setState( { errorMessage: 'Note should not be empty!' } );
		} else {
			fetch( valet_notes.route_url, {
				method: 'POST',
				headers: new Headers({
					'Authorization': 'Basic ' + encode( valet_notes.connect_info.user_login + ":" + valet_notes.connect_info.password ),
					'Content-Type': 'application/json'
				}),
				body: JSON.stringify( { note: this.state.note } ),
			})
			.then( response => response.json() )
			.then( res => {
				const { notes } = this.state;
				notes.unshift( res );

				this.setState( {
					total: this.state.total + 1,
					note: '',
					notes,
					successMessage: 'Note added!',
				} );
				setTimeout( () => {
					this.setState( { successMessage: '' } );
				}, 6000 );
			})
			.catch((error) => {
				console.error('Error:', error);
				alert(
					'Error in creating the note with the message: ' +
						error.message +
						'(' +
						error.code +
						')'
				);
			});
		}
	};

	handleCreateNoteChange = ( event ) => {
		const noteVal = event.target.value;
		const newState = { note: noteVal };
		if ( '' == this.state.note ) {
			newState.errorMessage = '';
		}
		this.setState( newState );
	};

	componentDidMount() {
		this.loadNotes();
	}

	updateContentOfNote = ( data ) => {
		const { notes } = this.state;
		const newNotes = notes.map( ( note, index ) => {
			if ( note.id == data.id ) {
				note.note = data.note;
			}
			return note;
		} );
		this.setState( {
			notes: newNotes,
		} );
	};

	loadNotes = () => {
		fetch( `${valet_notes.route_url}?search=${this.state.search}`, {
			method: 'GET',
			headers: new Headers({
				'Authorization': 'Basic ' + encode( valet_notes.connect_info.user_login + ":" + valet_notes.connect_info.password ),
				'Content-Type': 'application/json'
			}),
		})
		.then( response => {
			this.setState( { 
							total: response.headers.get( 'X-WP-Total' ),
						} );
			return response.json() 
		})
		.then( res => {
			this.setState( {
				notes: res,
				isLoading: false,
			} );
		})
		.catch((error) => {
			console.error('Error:', error);
			alert(
				'Error in fetching notes list with the message: ' +
					error.message +
					'(' +
					error.code +
					')'
			);
		});
	};

	handleDeleteNote = ( note_id ) => {
		if ( confirm( 'Are you sure you want to delete this note?' ) ) {
			fetch( valet_notes.route_url + '/' + note_id, {
				method: 'DELETE',
				headers: new Headers({
					'Authorization': 'Basic ' + encode( valet_notes.connect_info.user_login + ":" + valet_notes.connect_info.password ),
					'Content-Type': 'application/json'
				}),
			})
			.then( response => response.json() )
			.then( res => {
				if ( true === res ) {
					this.loadNotes();
				}
			})
			.catch((error) => {
				console.error('Error:', error);
				alert(
					'Error in deleting the note with the message: ' +
						error.message +
						'(' +
						error.code +
						')'
				);
			});
		}
	};

	handleSearchChange = ( event ) => {
		this.setState(
			{ 
				search: event.target.value
			}
		);
	}

	handleSearchKeyUp = ( event ) => {
		if ( 13 == event.keyCode ) {
			event.preventDefault();
			this.loadNotes();
		}
	}

	handleSearchClick = () => {
		this.loadNotes();
	}

	handleSearchClearClick = ( event ) => {
		event.preventDefault();
		this.setState(
			{ 
				search: ''
			},
			() => this.loadNotes()
		);
	}

	render() {
		// console.log( `Total: ${this.state.total}` );

		return (
			<Fragment>
				<h1> Notes </h1>
				<CreateNote
					// methods
					handleCreateNoteFormSubmit={
						this.handleCreateNoteFormSubmit
					}
					handleCreateNoteChange={ this.handleCreateNoteChange }
					// vars
					note={ this.state.note }
					errorMessage={ this.state.errorMessage }
					successMessage={ this.state.successMessage }
				/>
				<hr />
				{ false === this.state.isLoading && this.state.total > 0 &&
					(
						<Fragment>
							<input type="text" className="valet-support search-input-text" value={ this.state.search } placeholder="Search" onChange={ this.handleSearchChange } onKeyUp={ this.handleSearchKeyUp } size="64" />
							<Button isSecondary className="is-button valet-support search-button" isSmall={ true } onClick={ this.handleSearchClick }>Search</Button>
							<Button className="is-button valet-support search-button" isSmall={ true } isDestructive onClick={ this.handleSearchClearClick }>Clear</Button>
						</Fragment>
					)
				}
				<NotesList
					// methods
					handleDeleteNote={ this.handleDeleteNote }
					loadNotes={ this.loadNotes }
					updateContentOfNote={ this.updateContentOfNote }
					// vars
					notes={ this.state.notes }
					search={ this.state.search }
					isLoading={ this.state.isLoading }
				/>
			</Fragment>
		);
	}
}
