import { Component, Fragment } from '@wordpress/element';
import moment from 'moment';
import { encode } from "base-64";

import SingleNoteButtons from './SingleNoteButtons';
import EditNote from './EditNote';

export default class SingleNote extends Component {
	constructor( props ) {
		super( props );
		this.state = {
			note: '',
			editNote: '',
			isEditing: false,
			errorMessage: '',
		};
	}

	static getDerivedStateFromProps( props, state ) {
		return { note: props.note };
	}

	handleEditNote = ( note_id ) => {
		this.setState( {
			isEditing: true,
			editNote: this.state.note,
		} );
	};

	handleChangeNoteTextArea = ( event ) => {
		this.setState( {
			editNote: event.target.value,
		} );
	};

	handleUpdateNote = () => {
		const data = {
			id: this.props.id,
			note: this.state.editNote,
		};
		fetch( valet_notes.route_url + '/' + this.props.id, {
			method: 'PUT',
			headers: new Headers({
				'Authorization': 'Basic ' + encode( valet_notes.connect_info.user_login + ":" + valet_notes.connect_info.password ),
				'Content-Type': 'application/json'
			}),
			body: JSON.stringify( data ),
		})
		.then( response => response.json() )
		.then( res => {
			if ( true === res ) {
				this.props.updateContentOfNote( data );
				this.setState( {
					editNote: '',
					isEditing: false,
				} );
			}
		})
		.catch((error) => {
			console.error('Error:', error);
			alert(
				'Error in updating the note with the message: ' +
					error.message +
					'(' +
					error.code +
					')'
			);
		});
		/*
		apiFetch( {
			path: valet_notes.route_base + '/' + this.props.id,
			method: 'PUT',
			cache: 'no-cache',
			data,
		} ).then(
			( res ) => {
				if ( true === res ) {
					this.props.updateContentOfNote( data );
					this.setState( {
						editNote: '',
						isEditing: false,
					} );
				}
			},
			( error ) => {
				alert(
					'Error in updating the note with the message: ' +
						error.message +
						'(' +
						error.code +
						')'
				);
			}
		);
		*/
	};

	handleCancelEditNote = () => {
		this.setState( {
			isEditing: false,
		} );
	};

	render() {
		const { id, create_date } = this.props;
		const { note, isEditing, errorMessage } = this.state;
		const moment_local = moment.utc( create_date ).local();

		let ret;
		if ( isEditing ) {
			ret = (
				<EditNote
					// methods
					handleUpdateNote={ this.handleUpdateNote }
					handleCancelEditNote={ this.handleCancelEditNote }
					handleChangeNoteTextArea={ this.handleChangeNoteTextArea }
					// vars
					id={ id }
					editNote={ this.state.editNote }
					errorMessage={ errorMessage }
				/>
			);
		} else {
			ret = (
				<Fragment>
					<p> { this.state.note } </p>
					<SingleNoteButtons
						id={ id }
						handleDeleteNote={ this.props.handleDeleteNote }
						handleEditNote={ this.handleEditNote }
					/>
				</Fragment>
			);
		}

		return (
			<div className="card valet-note-single-cotainer" key={ id }>
				<p>
					Created at:{ ' ' }
					<strong>
						{ ' ' }
						{ moment_local.format(
							'MMMM Do YYYY, dddd, h:mm:ss a'
						) }{ ' ' }
					</strong>{ ' ' }
					({ moment_local.fromNow() })
				</p>
				{ ret }
			</div>
		);
	}
}
