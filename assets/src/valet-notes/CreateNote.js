import { Component, Fragment } from '@wordpress/element';
import { Notice } from '@wordpress/components';

import './styles/CreateNote.css';

export default class CreateNote extends Component {
	render() {
		return (
			<Fragment>
				{ this.props.successMessage && (
					<Notice status="success" isDismissible={ true }>
						<p> { this.props.successMessage } </p>
					</Notice>
				) }

				<form
					method="post"
					name="valet-support-note"
					id="valet-support-note"
					onSubmit={ this.props.handleCreateNoteFormSubmit }
				>
					<table className="form-table" role="presentation">
						<tbody>
							<tr>
								<th>
									<label htmlFor="valet-note"> Note </label>
									<span className="description">
										{ ' ' }
										(required){ ' ' }
									</span>
								</th>
								<td>
									<textarea
										onChange={
											this.props.handleCreateNoteChange
										}
										value={ this.props.note }
										id="valet-note"
										rows="5"
										cols="55"
									/>
									{ this.props.errorMessage && (
										<p className="valet-error">
											{ ' ' }
											{ this.props.errorMessage }{ ' ' }
										</p>
									) }
								</td>
							</tr>
						</tbody>
					</table>

					<p className="submit">
						<button type="submit" className="button button-primary">
							{ ' ' }
							Add Note{ ' ' }
						</button>
					</p>
				</form>
			</Fragment>
		);
	}
}
