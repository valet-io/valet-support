import { Component } from '@wordpress/element';
import { ButtonGroup, Button } from '@wordpress/components';

import SingleLinkButtons from './SingleLinkButtons';

export default class SingleLink extends Component {
	render() {
		const { id } = this.props;
		return (
			<ButtonGroup>
				<Button
					onClick={ () => this.props.handleEditLink() }
					isSmall={ true }
				>
					Edit
				</Button>
				<Button
					onClick={ () => this.props.handleDeleteLink( id ) }
					isPrimary
					isDestructive
					isSmall={ true }
				>
					Delete
				</Button>
			</ButtonGroup>
		);
	}
}
