import { Component } from '@wordpress/element';
import SingleNote from './SingleNote';
import { Spinner } from '@wordpress/components';

import './styles/NoteList.css';

export default class NotesLists extends Component {
	render() {
		const { notes, isLoading, search } = this.props;
		return (
			<div className="valet-note-cotainer">
				{ isLoading && (
					<div className="valet-notes-loader">
						{ ' ' }
						<Spinner color="blue" size="50" />{ ' ' }
					</div>
				) }

				{ ! isLoading && '' !== search && 0 == notes.length && 
					<div className="valet-support no-list">No note found!</div>
				}
				
				{ notes.map( ( note ) => (
					<SingleNote
						{ ...note }
						handleDeleteNote={ this.props.handleDeleteNote }
						updateContentOfNote={ this.props.updateContentOfNote }
					/>
				) ) }
			</div>
		);
	}
}
