import { Component, Fragment } from '@wordpress/element';
import apiFetch from '@wordpress/api-fetch';

import CreateNote from './CreateNote';
import NotesList from './LinksList';

export default class ValetNotes extends Component {
	constructor( props ) {
		super( props );
		this.state = {
			note: '',
			errorMessage: '',
			successMessage: '',
			notes: [],
			isLoading: true,
		};
	}

	handleCreateNoteFormSubmit = ( event ) => {
		event.preventDefault();
		const trimmedNote = this.state.note.trim();
		if ( '' == trimmedNote ) {
			this.setState( { errorMessage: 'Note should not be empty!' } );
		} else {
			apiFetch( {
				path: valet_notes.route_base,
				method: 'POST',
				cache: 'no-cache',
				data: { note: this.state.note },
			} ).then(
				( res ) => {
					const { notes } = this.state;
					notes.unshift( res );
					this.setState( {
						note: '',
						notes,
						successMessage: 'Note added!',
					} );
					setTimeout( () => {
						this.setState( { successMessage: '' } );
					}, 6000 );
				},
				( error ) => {
					alert(
						'Error in creating the note with the message: ' +
							error.message +
							'(' +
							error.code +
							')'
					);
				}
			);
		}
	};

	handleCreateNoteChange = ( event ) => {
		const noteVal = event.target.value;
		const newState = { note: noteVal };
		if ( '' == this.state.note ) {
			newState.errorMessage = '';
		}
		this.setState( newState );
	};

	componentDidMount() {
		this.loadNotes();
	}

	updateContentOfNote = ( data ) => {
		const { notes } = this.state;
		const newNotes = notes.map( ( note, index ) => {
			if ( note.id == data.id ) {
				note.note = data.note;
			}
			return note;
		} );
		this.setState( {
			notes: newNotes,
		} );
	};

	loadNotes = () => {
		apiFetch( {
			path: valet_notes.route_base,
			method: 'GET',
		} ).then(
			( res ) => {
				this.setState( {
					notes: res,
					isLoading: false,
				} );
			},
			( error ) => {
				this.setState( {
					isLoading: false,
				} );
				alert(
					'Error in fetching notes list with the message: ' +
						error.message +
						'(' +
						error.code +
						')'
				);
			}
		);
	};

	handleDeleteNote = ( note_id ) => {
		if ( confirm( 'Are you sure you want to delete this note?' ) ) {
			apiFetch( {
				path: valet_notes.route_base + '/' + note_id,
				method: 'DELETE',
			} ).then(
				( res ) => {
					if ( true === res ) {
						this.loadNotes();
					}
				},
				( error ) => {
					alert(
						'Error in deleting the note with the message: ' +
							error.message +
							'(' +
							error.code +
							')'
					);
				}
			);
		}
	};

	render() {
		return (
			<Fragment>
				<CreateNote
					// methods
					handleCreateNoteFormSubmit={
						this.handleCreateNoteFormSubmit
					}
					handleCreateNoteChange={ this.handleCreateNoteChange }
					// vars
					note={ this.state.note }
					errorMessage={ this.state.errorMessage }
					successMessage={ this.state.successMessage }
				/>
				<hr />
				<NotesList
					// methods
					handleDeleteNote={ this.handleDeleteNote }
					loadNotes={ this.loadNotes }
					updateContentOfNote={ this.updateContentOfNote }
					// vars
					notes={ this.state.notes }
					isLoading={ this.state.isLoading }
				/>
			</Fragment>
		);
	}
}
