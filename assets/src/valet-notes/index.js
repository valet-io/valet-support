import { render } from '@wordpress/element';
import ValetNotes from './ValetNotes';

if ( null !== document.getElementById( 'valet-support-notes-wrapper' ) ) {
	render(
		<ValetNotes />,
		document.getElementById( 'valet-support-notes-wrapper' )
	);
}
