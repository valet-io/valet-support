import { render } from '@wordpress/element';
import BackupLogs from './BackupLogs';

import './styles/index.css';

if ( null !== document.getElementById( 'valet-support-backup-logs-wrapper' ) ) {
	render(
		<BackupLogs />,
		document.getElementById( 'valet-support-backup-logs-wrapper' )
	);
}
