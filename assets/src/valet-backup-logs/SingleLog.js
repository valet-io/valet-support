import { Component, Fragment } from '@wordpress/element';
import Interweave from 'Interweave';
import moment from 'moment';
import apiFetch from '@wordpress/api-fetch';

export default class SingleLog extends Component {
	constructor( props ) {
		super( props );
	}

	render() {
		const moment_local = moment.utc( this.props.backup_date_time ).local();
		return (
			<div className="single-backup-log">
				<div className="single-log-first-container">
					<div>
						Backup Date Time:
						<strong>
							{ ' ' }
							{ moment_local.format(
								'MMMM Do YYYY, dddd, h:mm:ss a'
							) }{ ' ' }
						</strong>{ ' ' }
						({ moment_local.fromNow() })
					</div>
				</div>
			</div>
		);
	}
}
