=== Valet: Support ===
Stable tag: 0.0.1
Requires at least: 4.9
Tested up to: 4.9
Requires PHP: 5.3
Contributors: Valet
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Valet.io's custom plugin for advanced debugging and support.

== Description ==
Valet.io's custom plugin for advanced debugging and support.

== Installation ==
Upload through "Plugins" in your WordPress admin.