<?php
/**
 * Fired when the plugin is uninstalled.
 *
 * When populating this file, consider the following flow
 * of control:
 *
 * - This method should be static
 * - Check if the $_REQUEST content actually is the plugin name
 * - Run an admin referrer check to make sure it goes through authentication
 * - Verify the output of $_GET makes sense
 * - Repeat with other user roles. Best directly by using the links/query string parameters.
 * - Repeat things for multisite. Once for a single site in the network, once sitewide.
 *
 * This file may be updated more in future version of the Boilerplate; however, this is the
 * general skeleton and outline for how the file should work.
 *
 * @link       https://www.valet.io/
 * @since      1.0.3
 *
 * @package    Valet_Support
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

$timestamp = wp_next_scheduled( 'valet_cron_hook' );
wp_unschedule_event( $timestamp, 'valet_cron_hook' );

$table_names = array(
	$GLOBALS['wpdb']->base_prefix . 'valet_support_notes',
	$GLOBALS['wpdb']->base_prefix . 'valet_support_activity_log',
);
foreach ( $table_names as $table_name ) {
	$sql = "DROP TABLE IF EXISTS $table_name";
	$GLOBALS['wpdb']->query( $sql );
}


$options = array(
	'valet_support_plugins_versions',
	'valet_support_themes_versions',
	'valet_support_db_version',
	'valet_support_version',
);
foreach ( $options  as $option ) {
	delete_option( $option );
}
